tank_skin
{
	background
	{
		file { img/background_test.svg }
	}

	target
	{
		file { img/target_tank.svg }
	}

	cover
	{
		file { img/cover_test.svg }
	}

	shot
	{
		file { img/shot_tank.svg }
	}

	anti_shot
	{
		file { img/shot_tank.svg }
	}

	ready
	{
		file { img/ready_test.svg }
	}

	empty_cell
	{
		file { img/empty_cell_test.svg }
	}

	neg_empty_cell
	{
		file { img/neg_empty_cell_test.svg }
	}

	selected_cell
	{
		file { img/shot_tank.svg }
	}

	neg_selected_cell
	{
		file { img/shot_neg_tank.svg }
	}

	unused_cell
	{
		file { img/unused_cell_test.svg }
	}

	open_numsel
	{
		file { img/open_numselector_test.svg }
	}

	open_puzzle
	{
		file { img/puzzle_piece.svg }
	}

	hide_numsel
	{
		file { img/hide_numselector_test.svg }
	}

	progress_on
	{
		file { img/blast.svg }
	}

	progress_off
	{
		file { img/target_tank.svg }
	}

	puzzles
	{
		addition
		{
			bg
			{
				file { img/addition_bg_tmp.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_1.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_2.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_3.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_4.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_5.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_6.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_7.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_8.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_9.svg }
			}
			piece
			{
				file { img/addition_piece_tmp_10.svg }
			}
		}
		subtraction
		{
			bg
			{
				file { img/subtraction_bg_tmp.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_1.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_2.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_3.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_4.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_5.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_6.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_7.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_8.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_9.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_10.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_11.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_12.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_13.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_14.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_15.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_16.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_17.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_18.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_19.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_20.svg }
			}
			piece
			{
				file { img/subtraction_piece_tmp_21.svg }
			}
		}
		multiplication
		{
			bg
			{
				file { img/multiplication_bg_tmp.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_1.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_2.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_3.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_4.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_5.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_6.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_7.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_8.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_9.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_10.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_11.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_12.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_13.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_14.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_15.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_16.svg }
			}
			piece
			{
				file { img/multiplication_piece_tmp_17.svg }
			}
		}
		division
		{
			bg
			{
				file { img/division_bg_tmp.svg }
			}
			piece
			{
				file { img/division_piece_tmp_1.svg }
			}
			piece
			{
				file { img/division_piece_tmp_2.svg }
			}
			piece
			{
				file { img/division_piece_tmp_3.svg }
			}
			piece
			{
				file { img/division_piece_tmp_4.svg }
			}
			piece
			{
				file { img/division_piece_tmp_5.svg }
			}
			piece
			{
				file { img/division_piece_tmp_6.svg }
			}
			piece
			{
				file { img/division_piece_tmp_7.svg }
			}
			piece
			{
				file { img/division_piece_tmp_8.svg }
			}
			piece
			{
				file { img/division_piece_tmp_9.svg }
			}
			piece
			{
				file { img/division_piece_tmp_10.svg }
			}
			piece
			{
				file { img/division_piece_tmp_11.svg }
			}
			piece
			{
				file { img/division_piece_tmp_12.svg }
			}
			piece
			{
				file { img/division_piece_tmp_13.svg }
			}
			piece
			{
				file { img/division_piece_tmp_14.svg }
			}
			piece
			{
				file { img/division_piece_tmp_15.svg }
			}
			piece
			{
				file { img/division_piece_tmp_16.svg }
			}
			piece
			{
				file { img/division_piece_tmp_17.svg }
			}
		}
	}

	frame_collection
	{
		frame
		{
			name {"f1"}
			file { img/shot_tank.svg }
			dim
			{
				x {0.0625}
				y {0.0625}
			}
		}
		frame
		{
			name {"f2"}
			file { img/target_tank.svg }
			dim
			{
				x {0.1}
				y {0.1}
			}
		}
		frame
		{
			name {"f3"}
			file { img/blast.svg }
			dim
			{
				x {0.1}
				y {0.1}
			}
		}
	}

	shooting_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {1}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f2"}
			}
		}
		sequence
		{
			start_time {2000}
			end_time {3000}
			start_pos
			{
				trajectory {0}
				time {1}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			trajectory {0}
			frame_duration {1000}
			frame
			{
				name {"f3"}
			}
		}
	}
	annihilation_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {1}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
	}
	lonely_shot_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {0}
			}
			/* index in trajectory function array defined in code */
			trajectory {1}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
	}

	lonely_target_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {0}
			}
			/* index in trajectory function array defined in code */
			trajectory {1}
			frame_duration {100}
			frame
			{
				name {"f2"}
			}
		}
	}
}
