test_skin
{
	background
	{
		file { img/background_test.svg }
	}

	target
	{
		file { img/target_test.svg }
	}

	cover
	{
		file { img/cover_test.svg }
	}

	shot
	{
		file { img/shot_test.svg }
	}

	anti_shot
	{
		file { img/shot_test.svg }
	}

	ready
	{
		file { img/ready_test.svg }
	}

	empty_cell
	{
		file { img/empty_cell_test.svg }
	}

	neg_empty_cell
	{
		file { img/neg_empty_cell_test.svg }
	}

	selected_cell
	{
		file { img/selected_cell_test.svg }
	}

	neg_selected_cell
	{
		file { img/neg_selected_cell_test.svg }
	}

	unused_cell
	{
		file { img/unused_cell_test.svg }
	}

	open_numsel
	{
		file { img/open_numselector_test.svg }
	}

	hide_numsel
	{
		file { img/hide_numselector_test.svg }
	}


	frame_collection
	{
		frame
		{
			name {"f1"}
			file { img/shot_test.svg }
			dim
			{
				x {0.125}
				y {0.125}
			}
			/* file { img/f1.svg } */
		}
		frame
		{
			name {"f2"}
			file { img/target_test.svg }
			dim
			{
				x {0.125}
				y {0.125}
			}
			/* file { img/f2.svg } */
		}
	}
	
	shooting_animation
	{
		
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {1}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f2"}
			}
		}
	}
	annihilation_animation
	{
		
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {1}
			}
			end_pos
			{
				trajectory {0}
				time {1}
			}
			/* index in trajectory function array defined in code */
			trajectory {0}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
	}
	lonely_shot_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {0}
			}
			/* index in trajectory function array defined in code */
			trajectory {1}
			frame_duration {100}
			frame
			{
				name {"f1"}
			}
		}
	}

	lonely_target_animation
	{
		sequence
		{
			start_time {0}
			end_time {2000}
			start_pos
			{
				trajectory {0}
				time {0}
			}
			end_pos
			{
				trajectory {0}
				time {0}
			}
			/* index in trajectory function array defined in code */
			trajectory {1}
			frame_duration {100}
			frame
			{
				name {"f2"}
			}
		}
	}
}
