include prorab.mk

$(eval $(call prorab-config, config))

this_name := the-game

this_srcs += $(call prorab-src-dir,src)

# this_cxxflags += -D NO_SCROLL
# this_cxxflags += -D __PUZZLE__

ifeq ($(debug),true)
    this_cxxflags += -DDEBUG
endif

this_ldlibs += -lruisapp-opengl -lstdc++ -lruis -ltml -lnitki -lopros -lutki -lpulse-simple -laumiks -laudout -rdynamic -lm

$(eval $(prorab-build-app))
