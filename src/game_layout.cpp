#include <fstream>
#include <assert.h>
#include "game_layout.hpp"

const float game_layout::targ_grid_size = 0.09f;
const float game_layout::shot_grid_size = 0.06f;
const float game_layout::shot_grid_shift = 1.0f;

game_layout::game_layout()
{
	default_init();
	_state.push_back((int *)&_failed_layout.first);
	_state.push_back((int *)&_failed_layout.second);
	_state.push_back((int *)&_prev_layout.first);
	_state.push_back((int *)&_prev_layout.second);
	_state.push_back((int *)&_new_layout.first);
	_state.push_back((int *)&_new_layout.second);
	_state.push_back((int *)&_correcting_error);
	_state.push_back(&_right_answers);
	_state.push_back((int *)&_select_term_counter);
	_state.push_back((int *)&_gen_res.prize_level);
}

void game_layout::default_init()
{
	_select_term_counter = 0;
	_correcting_error = false;
	_gen_res.prize_level = false;
	_gen_res.show_prize = false;
}

number_selector::selector_mode game_layout::layout_type()
{
	return SEL_UNSIGNED;
}

game::game_gen_result game_layout::gen_layout(unsigned int &n_targets, unsigned int &n_shots)
{
	if (!_correcting_error)
		select_terms();
	target_count(n_targets, n_shots);
	_target_count = n_targets;
	terms_labels(_gen_res.first, _gen_res.second);
	_gen_res.mode = layout_type();
	_gen_res.progress = (stage_is_quiz()?
			     std::max(0,_right_answers):
			     _select_term_counter) * 10 / required_right_answers();
	return final_gen_res();
}

void game_layout::terms_labels(unsigned int &first, unsigned int &second)
{
	first = _new_layout.first;
	second = _new_layout.second;
}

game::game_gen_result game_layout::final_gen_res()
{
	game::game_gen_result res(_gen_res);
	_gen_res.show_prize = false;
	res.prize_level = get_prize_level();
	return res;
}

ruis::vector2 game_layout::target_position_base(unsigned int idx,
						 float gap)
{
	return ruis::vector2(gap*(10 - idx%10), gap*(idx/10) + 0.05);
}

ruis::vector2 game_layout::pre_shot_position(unsigned int idx)
{
	ASSERT(0)
	return ruis::vector2(0.0, 0.0);
}

ruis::vector2 game_layout::shot_position(unsigned int idx)
{
	ruis::vector2 p;
	ASSERT(idx < 100)
	p = target_position_base(idx, shot_grid_size);
	p.x()+=shot_grid_shift;
	return p;
}

void game_layout::game_result_was(int error, unsigned int time)
{
	if (error != 0)
	{
		if (!_correcting_error) {
			_failed_layout = _new_layout;
			_correcting_error = true;
		}
		_right_answers = (_right_answers > 0)? -1: _right_answers-1;
		if (_right_answers < -2) {
			simplify_layout();
		}
		return;

	}
	else
	{
		_prev_layout = _new_layout;
		if (_correcting_error)
		{
			_right_answers = 1;
			if (_new_layout != _failed_layout)
			{
				advance_to_failed();
				return;
			}
			else
			{
				_correcting_error = false;
				++_select_term_counter;
			}
		}
		else {
			++_select_term_counter;
			_right_answers = _right_answers < 0? 1: 1+_right_answers;
			if (stage_is_final()) {
				_right_answers =
					std::min(_right_answers, static_cast<int>(required_right_answers()));
			}
			if (stage_is_quiz() &&
			    _right_answers >= static_cast<int>(required_right_answers()))
			{
				_right_answers = 0;
				advance_stage();
			}
		}
	}
}

unsigned int game_layout::get_prize_level()
{
	return _gen_res.prize_level;
}

unsigned int game_layout::required_right_answers()
{
	return 10;
}

void game_layout::select_rnd_unisum(unsigned int n, unsigned int &x, unsigned int &y)
{
	unsigned int sum = rand() %n;
	x = rand()%(sum+1);
	y = sum - x;
}

void game_layout::select_rnd_unipairs(unsigned int n, unsigned int &x, unsigned int &y)
{
	x = rand()%n;
	y = rand()%(n+1);
	if (x + y > n - 1)
	{
		x = n - 1 - x;
		y = n - y;
	}
}

const game_layout::layout &
game_layout::layout::operator= (const game_layout::layout &l)
{
	first = l.first;
	second = l.second;
	return *this;
}

bool game_layout::layout::operator== (const game_layout::layout &l)
{
	return l.first == first && l.second == second;
}

bool game_layout::layout::operator!= (const game_layout::layout &l)
{
	return l.first != first || l.second != second;
}

void game_layout::layout::operator()(unsigned int f, unsigned int s)
{
	first = f;
	second = s;
}

std::ostream &operator << (std::ostream &file, game_layout &gl)
{
	for (auto &&var : gl._state)
		file << " " << *var;
	return file;
}

std::istream &operator >> (std::istream &file, game_layout &gl)
{
	int tmp;
	for (auto &&var : gl._state) {
		file >> tmp;
		if (file.rdstate() & std::ios::failbit) {
			gl.default_init();
			break;
		}
		*var = tmp;
	}
	return file;
}
