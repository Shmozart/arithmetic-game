#include <ruisapp/application.hpp>
#include <aumiks/wav_sound.hpp>
#include <tml/crawler.hpp>

#include "skin.hpp"

skin::skin(const utki::shared_ref<ruis::context>& c) :
	ruis::resource(c),
	_sink(audout::rate::hz_22050)
{
	_scaled_imgs.emplace_back(e_background, -1.0f);
	_scaled_imgs.emplace_back(e_target, 0.1f);
	_scaled_imgs.emplace_back(e_cover, 0.1f);
	_scaled_imgs.emplace_back(e_shot, 0.0625f);
	_sink.start();
}

bool skin::shooting_animations_over(unsigned int tm)
{
	for (auto && a : _shooting_animations)
	{
		if (a.unfinished(tm))
			return false;
	}
	for (auto && a : _lonely_shot_animations)
	{
		if (a.unfinished(tm))
			return false;
	}
	for (auto && a : _lonely_targ_animations)
	{
		if (a.unfinished(tm))
			return false;
	}
	return true;
}

std::shared_ptr<const ruis::res::image::texture> skin::get_scaled_img(images img_id)
{
	for (auto && img : _scaled_imgs)
	{
		if (img._idx == img_id)
		{
			ASSERT(img._scaled_img != nullptr)
			return img._scaled_img;
		}
	}
	ASSERT(false)
	return nullptr;
}

std::shared_ptr<ruis::res::image> skin::get_vect_img(images img_id)
{
	return _vect_imgs[img_id];
}

#ifdef __PUZZLE__
std::shared_ptr<ruis::res::image> skin::get_puzzle_image(game::game_type gt, unsigned int idx)
{
	if (_puzzle_imgs[gt][idx]._img == nullptr) {
		_puzzle_imgs[gt][idx]._img =
			ruis::res::image::load(*this->context, *_puzzle_imgs[gt][idx]._fi);
	}
	return _puzzle_imgs[gt][idx]._img;
}
#endif

ruis::vector2
skin::get_obj(unsigned int &obj_idx, std::vector<sequence> &animations,
	      const ruis::vector2 &target_start, const ruis::vector2 &shot_start,
	      unsigned int time, std::shared_ptr<const ruis::res::image::texture> &img)
{
	while (obj_idx < animations.size())
	{
		if (animations[obj_idx++].active(time))
		{
			return animations[obj_idx - 1].get_frame(time, target_start,
								 shot_start, img);
		}
	}
	obj_idx = 0xFFFFFFFFu;
	return ruis::vector2(-1.0f, -1.0f);
}


ruis::vector2
skin::get_shooting_obj(bool annihilation, unsigned int &obj_idx,
		       const ruis::vector2 &target_start, const ruis::vector2 &shot_start,
		       unsigned int time, std::shared_ptr<const ruis::res::image::texture> &img)
{
	if (annihilation)
		return get_obj(obj_idx, _annihilation_animations,
			       target_start, shot_start, time, img);
	else
		return get_obj(obj_idx, _shooting_animations,
			       target_start, shot_start, time, img);
}

ruis::vector2
skin::get_lon_shot_obj(unsigned int &obj_idx, const ruis::vector2 &shot_pos,
		       unsigned int time, std::shared_ptr<const ruis::res::image::texture> &img)
{
	return get_obj(obj_idx, _lonely_shot_animations, shot_pos, shot_pos, time, img);
}

ruis::vector2
skin::get_lon_targ_obj(bool annihilation, unsigned int &obj_idx,
			    const ruis::vector2 &target_pos, unsigned int time,
			    std::shared_ptr<const ruis::res::image::texture> &img)
{
	if (annihilation)
		return get_obj(obj_idx, _lonely_shot_animations,
			       target_pos, target_pos, time, img);
	else
		return get_obj(obj_idx, _lonely_targ_animations,
			       target_pos, target_pos, time, img);
}

void skin::start_playing(skin::sounds snd, bool loop)
{
	_sink.input.connect(_sounds[snd]->create_source(_sink.sampling_rate));
}

void skin::load_frames(std::shared_ptr<skin> skin,
			const tml::forest& desc, const papki::file &fi)
{
	const auto frame = tml::crawler(desc).to("frame_collection").get().children;
	for (auto& f : frame)
	{
		fi.set_path(tml::crawler(f.children).to("file").in().get().value.string);
		skin->_anim_frames.push_back(std::make_shared<skin_img>(tml::crawler(f.children).to("name").in().get().value.string,
									ruis::res::image::load(skin->context, fi).to_shared_ptr(),
									tml::crawler(f.children).to("dim").in().to("x").in().get().value.to_float(),
									tml::crawler(f.children).to("dim").in().to("y").in().get().value.to_float()));
	}
}

void skin::load_seq(std::vector<sequence> &seq,
		    std::shared_ptr<skin> skin,
		    const tml::forest& desc, const papki::file &fi,
		    const std::string anim_name)
{
	const auto& seq_list = tml::crawler(desc).to(anim_name).get().children;
	for (auto& s : seq_list)
	{
		if (s.value != "sequence")
			continue;

		unsigned int trck_idx = tml::crawler(s.children).to("trajectory").in().get().value.to_uint32();
		unsigned int start_tm = tml::crawler(s.children).to("start_time").in().get().value.to_uint32();
		unsigned int end_tm = tml::crawler(s.children).to("end_time").in().get().value.to_uint32();
		unsigned int frm_dur = tml::crawler(s.children).to("frame_duration").in().get().value.to_uint32();
		unsigned int start_trck_idx = tml::crawler(s.children).to("start_pos").in().to("trajectory").in().get().value.to_uint32();
		float start_trck_tm = tml::crawler(s.children).to("start_pos").in().to("time").in().get().value.to_float();
		unsigned int end_trck_idx = tml::crawler(s.children).to("end_pos").in().to("trajectory").in().get().value.to_uint32();
		float end_trck_tm = tml::crawler(s.children).to("end_pos").in().to("time").in().get().value.to_float();
		seq.emplace_back(trck_idx,
					      start_trck_idx,
					      start_trck_tm,
					      end_trck_idx,
					      end_trck_tm,
					      start_tm,
					      end_tm,
					      frm_dur);
		for (auto& f : s.children)
		{
			if (f.value != "frame")
				continue;

			seq.back().add_frame(skin->frame_by_name(tml::crawler(f.children).in().get().value.string));
		}
	}
}

#ifdef __PUZZLE__
std::vector<skin::lazy_img>
skin::load_puzzle(const tml::tree& desc, const papki::file &fi)
{
	std::vector<skin::lazy_img> res;

	auto i = std::find(desc.children.begin(), desc.children.end(), "bg");
	ASSERT(i != desc.children.end())

	for (; i != desc.children.end(); ++i)
	{
		res.push_back(skin::lazy_img(fi, tml::crawler(i->children).to("file").up().get().value.to_string().c_str()));
	}

	return res;
}
#endif


utki::shared_ref<skin> skin::load(const utki::shared_ref<ruis::context>& ctx, const tml::forest& desc, const papki::file& fi)
{
	unsigned int i;
	static const std::array<std::string, e_img_count> img_res_names =
		{
			"background",
			"target",
			"cover",
			"shot",
			"anti_shot",
			"open_numsel",
			"empty_cell",
			"neg_empty_cell",
			"selected_cell",
			"neg_selected_cell",
			"unused_cell",
			"hide_numsel",
			"ready",
			"open_puzzle",
			"progress_on",
			"progress_off"
		};
#ifdef __PUZZLE__
	static const std::array<std::string, game::e_game_count>puzzle_names =
		{
			"addition",
			"subtraction",
			"multiplication",
			"division"
		};
#endif
	auto ret = utki::make_shared<skin>(ctx);

	for (i = 0; i < e_img_count; ++i) {
		fi.set_path(tml::crawler(desc).to(img_res_names[i]).in().to("file").in().get().value.string);
		ret.get()._vect_imgs[i] = ruis::res::image::load(ctx, fi).to_shared_ptr();
	}
#ifdef __PUZZLE__
	for (i = 0; i < game::e_game_count; ++i)
	{
		ret->_puzzle_imgs.push_back(load_puzzle(tml::crawler(desc).to("puzzles").up().to(puzzle_names[i]).get(), fi));
	}
#endif
	load_frames(ret.to_shared_ptr(), desc, fi);

	load_seq(ret.get()._shooting_animations, ret.to_shared_ptr(), desc, fi, "shooting_animation");
	load_seq(ret.get()._annihilation_animations, ret.to_shared_ptr(), desc, fi, "annihilation_animation");
	load_seq(ret.get()._lonely_shot_animations, ret.to_shared_ptr(), desc, fi, "lonely_shot_animation");
	load_seq(ret.get()._lonely_targ_animations, ret.to_shared_ptr(), desc, fi, "lonely_target_animation");
	return ret;
}

void skin::set_screen_dim(ruis::vector2 dim)
{
	if (dim == cur_dim){
		return;
	}
	cur_dim = dim;
	for (auto &img : _scaled_imgs)
	{
		if (img._scale_factor > 0.0f)
			img._scaled_img = _vect_imgs[img._idx]->
				get(ruis::vector2(dim.y()*img._scale_factor,
						 dim.y()*img._scale_factor)).to_shared_ptr();
		else
			img._scaled_img = _vect_imgs[img._idx]->get(dim).to_shared_ptr();
	}
	for (auto && img: _anim_frames)
	{
		img->_img = img->_vect_img->get(ruis::vector2(dim.y()*img->_dim.first, dim.y()*img->_dim.second)).to_shared_ptr();
	}
}

std::shared_ptr<skin_img> skin::frame_by_name(std::string name)
{
	for (auto && it: _anim_frames) {
		if (it->_name == name)
			return it;
	}
	ASSERT(false)
	return nullptr;
}
