#pragma once

#include "game_layout.hpp"

class game_division : public game_layout
{
#ifndef __PUZZLE__
public:
#endif
	enum stage
	{
		e_no_remainder_2,
		e_remainder_2,
		e_quiz_2,
		e_no_remainder_3,
		e_remainder_3,
		e_quiz_3,
		e_no_remainder_4,
		e_remainder_4,
		e_quiz_4,
		e_no_remainder_5,
		e_remainder_5,
		e_quiz_5,
		e_no_remainder_6,
		e_remainder_6,
		e_quiz_6,
		e_no_remainder_7,
		e_remainder_7,
		e_quiz_7,
		e_no_remainder_8,
		e_remainder_8,
		e_quiz_8,
		e_no_remainder_9,
		e_remainder_9,
		e_quiz_9,
		e_final_quiz,
		e_stage_count
	};
public:
	virtual ~game_division();
	game_division();

	virtual ruis::vector2 target_position(unsigned int idx,
					       game::stage stage,
					       bool &annihilation) override;

protected:
	virtual number_selector::selector_mode layout_type() override;
	virtual void target_count(unsigned int &n_targets, unsigned int &n_shots) override;
	virtual void default_init() override;
	virtual void select_terms() override;
	virtual void advance_stage() override;
	virtual void simplify_layout() override;
	virtual void advance_to_failed() override;
	virtual bool stage_is_quiz() override;
	virtual bool stage_is_final() override;
	virtual unsigned int required_right_answers() override;

private:
	unsigned int fixed_term();
	bool has_remainder();
};
