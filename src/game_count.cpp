#ifdef BLABLABLA
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "game_count.hpp"

game_count::~game_count(){}
game_count::game_count()
{
	unsigned int i;
	_target_positions.reserve(100);
	for (i = 0; i < 100; ++i)
		_target_positions.push_back(position(i));
	_level = 0;
	_results = 0;
	for (i = 0; i < level_count - 1; ++i) {
		_level_bounds[i].first = i*5/2;
		_level_bounds[i].second = _level_bounds[i].first + 5;
	}
	_level_bounds[level_count-1].first = 0;
	_level_bounds[level_count - 1].second = 25;
}

void game_count::store(std::vector<std::uint8_t> &saved_state)
{}

bool game_count::restore(const utki::Buf<std::uint8_t> saved_state,
			 size_t &offset)
{
	return true;
}

number_selector::selector_mode game_count::gen_layout(unsigned int &n_targets, unsigned int &n_shots)
{
	if (_results < -2)
		--(*this);
	else if (_results > 10)
		++(*this);
	if (_results < 0)
		n_targets = _target_count;
	else {
		n_targets = (rand()%(_level_bounds[_level].second - _level_bounds[_level].first)) + 1 + _level_bounds[_level].first;
		_target_count = n_targets;
	}
	std::cout << "Lvl: " << _level << ", R: " << _results << std::endl;
	n_shots = 0;
	shuffle(n_targets);
	return SEL_UNSIGNED;
}

void game_count::shuffle(unsigned int n)
{
	std::vector<std::pair<float, float>>::iterator begin, r;
	begin = _target_positions.begin();
	size_t left = _target_positions.size();
	while (n--) {
		r = begin;
		std::advance(r, rand()%left);
		std::swap(*begin, *r);
		++begin;
		--left;
	}
}

void game_count::game_result_was(int error, unsigned int time)
{
	std::cout << "Game ERR: " << error << std::endl;
	if (error != 0)
	{
		if (_results > 0)
			_results = -1;
		else
			--_results;
	}
	else if (_results < 0)
		_results = 1;
	else
		++_results;
}

void game_count::operator--(void)
{
	if (_level > 0)
		--_level;
	_results = 0;
}

void game_count::operator++(void)
{
	if (_level < level_count - 1)
		++_level;
	_results = 0;
}

std::pair<float, float> game_count::position(unsigned int i)
{
	static float t = 0.1f, l = 0.1f;
	static float w = 0.075f;
	static float h = w * sqrt(.75f);
	float x = l + w*(i%10) + (((i/10)%2 == 0)?(w*0.5f):0.0f);
	float y = t + h*(i/10);
	return std::make_pair(x, y);
}

bool game_count::target_position(unsigned int idx,
			     game::stage stage,
			     float &x,
			     float &y)
{
	x = _target_positions[idx].first;
	y = _target_positions[idx].second;
	return false;
}
#endif
