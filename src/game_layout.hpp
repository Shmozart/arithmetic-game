#pragma once

#include <vector>
#include "number_selector.hpp"
#include "game.hpp"

class game_layout
{
	friend class game;
protected:
	struct layout
	{
		unsigned int first;
		unsigned int second;
		const layout &operator= (const layout &l);
		bool operator== (const layout &l);
		bool operator!= (const layout &l);
		void operator()(unsigned int f, unsigned int s);
	};

public:
	game_layout();
	virtual ~game_layout() {};

	//Returns the number of generated targets and pre-generated shots
	game::game_gen_result gen_layout(unsigned int &n_targets,
						  unsigned int &n_shots);
	virtual number_selector::selector_mode layout_type();
	// return true if target is hidden
	virtual ruis::vector2 target_position(unsigned int idx,
					       game::stage stage,
					       bool &annihilation) = 0;
	ruis::vector2 target_position_base(unsigned int idx,
					    float gap=targ_grid_size);
	/* pre-generated shot position */
	virtual ruis::vector2 pre_shot_position(unsigned int idx);
	/* answer shot positions */
	virtual ruis::vector2 shot_position(unsigned int idx);

	virtual void game_result_was(int error, unsigned int time);

	virtual unsigned int get_prize_level();
	friend std::ostream &operator << (std::ostream &file, game_layout &gl);
	friend std::istream &operator >> (std::istream &file, game_layout &gl);

protected:
	virtual void default_init();
	virtual void target_count(unsigned int &n_targets, unsigned int &n_shots) = 0;
	virtual void select_terms() = 0;
	virtual void advance_stage() = 0;
	virtual void simplify_layout() = 0;
	virtual void advance_to_failed() = 0;
	virtual bool stage_is_quiz() = 0;
	virtual bool stage_is_final() = 0;
	virtual unsigned int required_right_answers();
	// game deduction calculates first and second differently
	virtual void terms_labels(unsigned int &first, unsigned int &second);
	game::game_gen_result final_gen_res();

	void select_rnd_unisum(unsigned int n, unsigned int &x, unsigned int &y);
	void select_rnd_unipairs(unsigned int n, unsigned int &x, unsigned int &y);


protected:
	unsigned int _target_count;
	layout _prev_layout;
	layout _new_layout;
	layout _failed_layout;
	bool _correcting_error;
	int _right_answers;
	unsigned int _select_term_counter;
	static const float targ_grid_size;
	static const float shot_grid_size;
	static const float shot_grid_shift;
	game::game_gen_result _gen_res;
	// variables to store
	std::vector<int *> _state;
};
