#include "field.hpp"
#include "skin.hpp"

field::field(const utki::shared_ref<ruis::context>& c, const tml::forest& desc) : widget(c, desc)
{
}

void field::set_engine(std::weak_ptr<engine> engine)
{
	_eng = engine;
}

void field::render(const ruis::matrix4 &matrix) const
{
	_eng.lock()->get_skin()->set_screen_dim(rect().d);

	ruis::matrix4 m = matrix;
	m.scale(rect().d);
	_eng.lock()->get_skin()->get_scaled_img(skin::e_background)->render(m);
	game::game_object obj;
	_eng.lock()->get_game()->reset_object_iteration();
	while (_eng.lock()->get_game()->get_next_object(obj))
	{
		ruis::vector2 bottom_left = img_bl(obj);
		m = matrix;
		m.translate(std::round(bottom_left.x()), std::round(bottom_left.y()));
		m.scale(obj._img->dims);
		obj._img->render(m);
	}
}

#ifdef NO_SCROLL
void field::set_animation(unsigned int period)
#else
void field::set_animation(unsigned int period, bool have_preshots)
#endif
{
	if (period == 0)
	{
#ifndef NO_SCROLL
		_with_shots = have_preshots;
#endif
		_in_animation = false;
		this->context.get().updater.get().stop(*this); // stop updating
		// render one more time to show changes in game
		this->context.get().run_from_ui_thread([](){});
	}
	else
	{
#ifndef NO_SCROLL
		_with_shots = true;
#endif
		_in_animation = true;
		this->context.get().updater.get().start(
				utki::make_weak(
						std::dynamic_pointer_cast<ruis::updateable>(this->shared_from_this())
					),
				period
			);
	}
	this->invalidate_layout();
}

void field::update(std::uint32_t dtMs)
{
	ASSERT(_in_animation)
	_eng.lock()->get_game()->increment_time(dtMs);
}

#ifndef NO_SCROLL
ruis::vector2 field::measure(const ruis::vector2 &quotum) const
{
	ruis::vector2 res;
	res.y() = quotum.y();
	res.x() = (_with_shots?1.7f:1.0f)*quotum.y();
	return res;
}
#endif

ruis::vector2 field::img_bl(const game::game_object &obj) const
{
	ruis::vector2 wh = rect().d;
	ruis::vector2 P(wh.x()-wh.y()*obj._position.x(), wh.y()*obj._position.y());
	return P - obj._img->dims*0.5f;
}
