#include <ruis/layouts/pile_layout.hpp>
#include <ruis/layouts/linear_layout.hpp>

#include "progress.hpp"
#include "skin.hpp"

namespace{
const auto progress_c = tml::read(R"qwertyuiop(
		id { progress_row }
		lp
		{
			dx { min }
			dy { fill }
		}
	)qwertyuiop");

const auto progress_item_c = R"qwertyuiop(
		@image {
			keep_aspect_ratio {true}
			lp
			{
				dx {min}
				dy {fill}
			}
		}
	)qwertyuiop";
// const auto progress_c = stob::parse(R"qwertyuiop(
//		@text
//		{
//			name { numbers_table }
//			lp
//			{
//				dx { fill }
//				dy { fill }
//			}
//		}
//	)qwertyuiop");
}

progress::progress(const utki::shared_ref<ruis::context>& c, const tml::forest& desc) :
		widget(std::move(c), desc),
		container(this->context, desc, ruis::layout::pile)
{
	unsigned int i;
	auto row = utki::make_shared<ruis::container>(this->context, progress_c, ruis::layout::row);
	for (i = 0; i < resolution; ++i) {
		auto sr = this->context.get().inflater.inflate_as<ruis::image>(progress_item_c);
		_imgs[i] = sr.to_shared_ptr();
		row.get().push_back(sr);
	}
	push_back(row);
}

void progress::set_progress(unsigned int progress, std::shared_ptr<skin> skin)
{
	ASSERT(progress <= resolution)

	unsigned int i;
	for (i = 0; i < resolution; ++i)
		_imgs[i]->set_image(skin->get_vect_img((i < progress) ?
						      skin::e_progress_on :
						      skin::e_progress_off));
}
