#pragma once

#include <vector>
#include <ruis/config.hpp>
#include <ruis/res/image.hpp>
#include "number_selector.hpp"

class engine;
class game_layout;

class game
{
	friend class game_layout;
	friend class game_addition;
	friend class game_multiplication;
	friend class game_subtraction;
	friend class game_division;
	friend class skin;
public:
	struct game_gen_result
	{
		unsigned int progress;
		unsigned int first;
		unsigned int second;
		number_selector::selector_mode mode;
		bool show_prize;
		unsigned int prize_level;
	};

	struct game_object
	{
		ruis::vector2 _position;
		std::shared_ptr<const ruis::res::image::texture> _img;
	};

	enum obj_group
	{
		e_targets,
		e_pre_shots,
		e_shots,
		e_group_count
	};

	struct deciding_iterator
	{
		unsigned int _obj_idx;
		unsigned int _obj_grp;
	};

	struct shooting_pair
	{
		unsigned int _target_idx;
		unsigned int _shot_idx;
		bool _pre_shot;
	};

	struct shooting_iterator
	{
		shooting_pair _pair;
		unsigned int _obj_idx;
		unsigned int _pair_count;
	};

	union iterator
	{
		deciding_iterator _deciding;
		shooting_iterator _shooting;
	};

	enum game_type
	{
		e_addition,
		e_subtraction,
		e_multiplication,
		e_division,
		e_game_count
	};

protected:
	enum stage
	{
		e_deciding,
		e_shooting,
		e_greeting,
		e_prix
	};
public:
	game(engine &eng);
	void reset_object_iteration();
	bool get_next_object(game_object &obj);
	void make_decision(int decision);
	void increment_time(std::uint32_t dtMs);
	void start_game(game_type type);
	friend std::istream &operator >> (std::istream &file, game &gm);
	friend std::ostream &operator << (std::ostream &file, game &gm);
	const std::string op_label(game_type type);

private:
	void increment_iterator();
	bool get_next_object_deciding(game_object &obj);
	bool get_next_object_shooting(game_object &obj);

private:
	engine &_eng;
	game_type _layout = e_addition;
	stage _stage;
	iterator _iter;

	// bool _positive_answer;
	std::array<unsigned int, e_group_count> _obj_count;
	unsigned int _animation_time;
	unsigned int _lonely_start;
	unsigned int _pair_delay;
	unsigned int _start_time;
	int _error;
	std::array<std::shared_ptr<game_layout>, e_game_count> _games;
	const std::array<std::string, e_game_count> _op_labels = {"+", "-", "*", "/"};
};
