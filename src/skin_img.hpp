#pragma once

#include <ruis/res/image.hpp>
#include <utki/debug.hpp>

struct skin_img
{
	std::string _name;
	std::shared_ptr<ruis::res::image> _vect_img;
	std::shared_ptr<const ruis::res::image::texture> _img;
	std::pair<float, float> _dim;

	skin_img(std::string name, std::shared_ptr<ruis::res::image> vect_img,
		 float dim_x, float dim_y):
		_name(name), _vect_img(vect_img) {
		ASSERT(_vect_img != nullptr)
		_dim = std::make_pair(dim_x, dim_y);
	};
};
