#pragma once

#include "game_layout.hpp"

class game_addition : public game_layout
{
public:
	virtual ~game_addition();
	game_addition();

	virtual ruis::vector2 target_position(unsigned int idx,
					       game::stage stage,
					       bool &p) override;

#ifndef __PUZZLE__
public:
#else
protected:
#endif
	enum stage
	{
		e_explain_under_ten,
		e_quiz_under_ten,
		e_explain_over_ten,
		e_quiz_over_ten,
		e_quiz_no_tens,
		e_tens_explain_under_ten,
		e_tens_quiz_under_ten,
		e_tens_explain_over_ten,
		e_tens_quiz_over_ten,
		e_quiz_with_tens,
		e_stage_count
	};

protected:
	virtual void target_count(unsigned int &n_targets, unsigned int &n_shots) override;
	virtual void default_init() override;
	virtual void select_terms() override;
	virtual void advance_stage() override;
	virtual void simplify_layout() override;
	virtual void advance_to_failed() override;
	virtual bool stage_is_quiz() override;
	virtual bool stage_is_final() override;
	virtual unsigned int required_right_answers() override;
};
