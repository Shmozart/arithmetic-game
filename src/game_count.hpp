#pragma once

#ifdef BLABLABLA
#include <vector>
#include "game_layout.hpp"

class game_count : public game_layout
{
public:
	virtual ~game_count();
	game_count();

	virtual void store(std::vector<std::uint8_t> &saved_state);
	virtual bool restore(const utki::Buf<std::uint8_t> saved_state,
			     size_t &offset);
	virtual number_selector::selector_mode gen_layout(unsigned int &n_targets, unsigned int &n_shots);

	virtual void game_result_was(int error, unsigned int time);
	virtual bool target_position(unsigned int idx,
				     game::stage stage,
				     float &x,
				     float &y);
private:
	void shuffle(unsigned int n);
	std::pair<float, float> position(unsigned int i);
	void operator--(void);
	void operator++(void);

private:
	std::vector<std::pair<float, float>> _target_positions;
	static const unsigned int level_count = 10;
	unsigned int _target_count;
	unsigned int _level;
	std::array<std::pair<unsigned int, unsigned int>, level_count> _level_bounds;
	int _results;
};
#endif
