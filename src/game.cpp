#include <fstream>
#include "game.hpp"
#include "skin.hpp"
#include "field.hpp"
#include "game_addition.hpp"
#include "game_multiplication.hpp"
#include "game_subtraction.hpp"
#include "game_division.hpp"
#include "ui.hpp"

game::game(engine &eng) : _eng(eng), _pair_delay(1000)
{
	srand(100);
	_games[e_addition] = std::make_shared<game_addition>();
	_games[e_multiplication] = std::make_shared<game_multiplication>();
	_games[e_subtraction] = std::make_shared<game_subtraction>();
	_games[e_division] = std::make_shared<game_division>();
}

void game::start_game(game_type type)
{
	number_selector::selector_mode sel_mod;
	_layout = type;
	_stage = e_deciding;
	_obj_count[e_shots] = 0;
	_eng.get_ui()->on_game_start(_games[_layout]->gen_layout(_obj_count[e_targets],
								 _obj_count[e_pre_shots]));
}

void game::make_decision(int decision)
{
	_stage = e_shooting;
	_animation_time = 0;
	_error = decision + _obj_count[e_pre_shots] - _obj_count[e_targets];
	if (decision >= 0)
		_obj_count[e_shots] = decision;
	else
		_obj_count[e_targets] -= decision;
	unsigned int total_shots = _obj_count[e_shots] + _obj_count[e_pre_shots];
	unsigned int pair_count = std::min(total_shots, _obj_count[e_targets]);
	if (total_shots > 0)
		_pair_delay = 2000/pair_count;
	_lonely_start = pair_count * _pair_delay;
	_eng.get_ui()->on_game_decision();
}

void game::increment_time(std::uint32_t dtMs)
{
	_animation_time += dtMs;
	switch (_stage)
	{
	case e_shooting:
		if (_eng.get_skin()->shooting_animations_over(_animation_time < _lonely_start ? 0 :
							      _animation_time - _lonely_start))
		{
			_games[_layout]->game_result_was(_error, 0);
			start_game(_layout);
		}
		break;
	default:
		break;
	}
}

void game::reset_object_iteration()
{
	switch (_stage)
	{
	case e_deciding:
		_iter._deciding._obj_idx = 0;
		_iter._deciding._obj_grp = e_targets;
		break;
	case e_shooting:
		_iter._shooting._obj_idx = 0;
		_iter._shooting._pair._target_idx = 0;
		_iter._shooting._pair._shot_idx = 0;
		_iter._shooting._pair._pre_shot = _obj_count[e_pre_shots] > 0;
		_iter._shooting._pair_count =
			std::max(_obj_count[e_pre_shots] + _obj_count[e_shots],
				 _obj_count[e_targets]);
		break;
	default:
		break;
	}
}

bool game::get_next_object(game_object &obj)
{
	switch (_stage)
	{
	case e_deciding:
		return get_next_object_deciding(obj);
		break;
	case e_shooting:
		return get_next_object_shooting(obj);
		break;
	default:
		return false;
		break;
	}
}

bool game::get_next_object_shooting(game_object &obj)
{
	ruis::vector2 t_start(-1.0f, -1.0f) ,s_start(-1.0f, -1.0f);
	shooting_iterator &iter = _iter._shooting;
	unsigned int tidx = iter._pair._target_idx;
	unsigned int sidx = iter._pair._shot_idx;
	bool annihilation = false;
	if (tidx >= iter._pair_count)
		return false;
	if (tidx < _obj_count[e_targets])
		t_start = _games[_layout]->target_position(tidx, _stage, annihilation);
	if (!iter._pair._pre_shot &&
	    sidx < _obj_count[e_shots] )
		s_start = _games[_layout]->shot_position(sidx);
	else if (iter._pair._pre_shot)
		s_start = _games[_layout]->pre_shot_position(sidx);
	unsigned int lonely_time = _animation_time < _lonely_start ? 0 :
		_animation_time - _lonely_start;
	if (t_start.x() < 0.0f)
	{
		/* too much shots */
		obj._position = _eng.get_skin()->get_lon_shot_obj(iter._obj_idx, s_start,
								  lonely_time, obj._img);
	}
	else if (s_start.x() < 0.0f)
	{
		/* not enough shots */
		obj._position = _eng.get_skin()->get_lon_targ_obj(annihilation,iter._obj_idx, t_start,
								  lonely_time, obj._img);
	}
	else
	{
		// exact match
		unsigned int pair_time = _animation_time < tidx * _pair_delay ? 0 :
			_animation_time - tidx * _pair_delay;
		obj._position = _eng.get_skin()->get_shooting_obj(annihilation,
								  iter._obj_idx,
								  t_start, s_start,
								  pair_time, obj._img);
	}
	if (iter._obj_idx == 0xFFFFFFFF)
	{
		// there was no object, try one more time.
		increment_iterator();
		return get_next_object_shooting(obj);
	}
	increment_iterator();
	return true;
}

bool game::get_next_object_deciding(game_object &obj)
{
	if (_iter._deciding._obj_grp == e_group_count)
		return false;
	switch (_iter._deciding._obj_grp)
	{
	case e_targets:
		bool annihilation;
		obj._position = _games[_layout]->target_position(_iter._deciding._obj_idx,
								 _stage, annihilation);
		if (annihilation)
			obj._img = _eng.get_skin()->get_scaled_img(skin::e_cover);
		else
			obj._img = _eng.get_skin()->get_scaled_img(skin::e_target);
		break;
	case e_pre_shots:
		obj._img = _eng.get_skin()->get_scaled_img(skin::e_shot);
		obj._position = _games[_layout]->pre_shot_position(_iter._deciding._obj_idx);
		break;
	case e_shots:
		obj._img = _eng.get_skin()->get_scaled_img(skin::e_shot);
		obj._position = _games[_layout]->shot_position(_iter._deciding._obj_idx);
		break;
	}
	increment_iterator();
	return true;
}

void game::increment_iterator()
{
	switch (_stage)
	{
	case e_deciding:
		++_iter._deciding._obj_idx;
		while (_iter._deciding._obj_grp < e_group_count && _iter._deciding._obj_idx >=
		       _obj_count[_iter._deciding._obj_grp])
		{
			_iter._deciding._obj_idx = 0;
			++_iter._deciding._obj_grp;
		}
		break;
	case e_shooting:
		if (_iter._shooting._obj_idx == 0xFFFFFFFF)
		{
			_iter._shooting._obj_idx = 0;
			++_iter._shooting._pair._target_idx;
			if (_iter._shooting._pair._pre_shot &&
			    _iter._shooting._pair._shot_idx == _obj_count[e_pre_shots] - 1)
			{
				_iter._shooting._pair._pre_shot = false;
				_iter._shooting._pair._shot_idx = 0;
			}
			else
			{
				++_iter._shooting._pair._shot_idx;
			}
		}
		break;
	default:
		break;
	}
}

std::istream& operator>>(std::istream &file, game &gm)
{
	int tmp;
	file >> tmp;
	bool read_continue = true;
	if (file.rdstate() & std::ios::failbit) {
		std::cout << "GAME nothing to restore" << std::endl;
		LOG([](auto&o){o << "####<<<< GAME nothing to restore" << std::endl;});
		tmp = game::e_addition;
		read_continue = false;
	}
	for (unsigned i = 0; i < game::e_game_count; ++i) {
		if (read_continue) {
			file >> *gm._games[i];
			std::cout << "restore layout" << std::endl;

			if (file.rdstate() & std::ios::failbit) {
				std::cout << "cannot restore" << std::endl;
				read_continue = false;
			}
		}
		gm._eng.get_ui()->set_prize_level((game::game_type)i, gm._games[i]->get_prize_level());
	}
	srand(time(NULL));
	gm._eng.get_ui()->set_op((game::game_type)tmp);
	gm.start_game((game::game_type)tmp);
	LOG([&](auto&o){o << "####<<<< Restored gametype: " << gm._layout << std::endl;});
	return file;
}

std::ostream &operator<<(std::ostream &file, game &gm)
{
	std::cout << "store games" << std::endl;
	file << gm._layout;
	LOG([&](auto&o){o << "####>>>> Save gametype: " << gm._layout << std::endl;});
	for (unsigned i = 0; i < game::e_game_count; ++i)
		file << *gm._games[i];
	return file;
}

const std::string game::op_label(game_type type)
{
	return _op_labels[type];
}
