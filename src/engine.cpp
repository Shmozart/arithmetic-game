#include <fstream>
#include "engine.hpp"
#include "ui.hpp"
#include "skin.hpp"

const std::string store_fname = ".store";

engine::engine(const std::string storage_dir) : _storage_dir(storage_dir)
{
	_game = std::make_shared<game>(*this);
}

void engine::init(std::shared_ptr<ui> ui)
{
	_ui = ui;
	_ui->set_engine(shared_from_this());
	load_settings();
	std::ifstream store_file;
	store_file.open(_storage_dir + std::string(store_fname), std::ios::binary);
	store_file >> *_game;
	store_file.close();
}

engine::~engine()
{
	std::cout << "store to: " << _storage_dir + std::string(store_fname) << std::endl;
	std::ofstream store_file;
	store_file.open(_storage_dir + std::string(store_fname), std::ios::binary);
	store_file << *_game;
	store_file.close();
}

void engine::load_settings()
{
	// load_skin("test_skin");
	load_skin("tank_skin");
}

void engine::load_skin(const std::string skin_res)
{
	_skin = _ui->context.get().loader.load<skin>(skin_res).to_shared_ptr();
	_ui->update_skin();
}

std::shared_ptr<game> engine::get_game()
{
	return _game;
}

std::shared_ptr<skin> engine::get_skin()
{
	return _skin;
}

std::shared_ptr<ui> engine::get_ui()
{
	return _ui;
}
