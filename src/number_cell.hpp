#pragma once

#include <ruis/widgets/button/image_toggle.hpp>
#include <ruis/widgets/label/text.hpp>
#include <ruis/res/image.hpp>

class number_cell : public ruis::container
{
	friend class number_selector;
public:
	number_cell(const utki::shared_ref<ruis::context>& c, int value);
	void operator=(int value);

	virtual ~number_cell();
	void set_selected(bool selected);
	int value() { return _value; }

private:
	int _value;
	std::shared_ptr<ruis::image_toggle> _button;
	std::shared_ptr<ruis::text> _label;
};
