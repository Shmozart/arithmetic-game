#pragma once

#include <ruis/widgets/group/scroll_area.hpp>
#include <ruis/widgets/button/drop_down_box.hpp>
#include "number_selector.hpp"
#include "progress.hpp"
#include "game.hpp"

class field;
class prize_puzzle;
namespace morda
{
	class text;
	class widget;
#ifdef NO_SCROLL
	class image_button;
#endif
};

class ui : public ruis::container
{
	class opsel_model : public ruis::drop_down_box::provider
	{
	public:
		opsel_model(std::weak_ptr<engine> eng);
		virtual ~opsel_model(){};
		virtual size_t count() const noexcept override;
		virtual utki::shared_ref<ruis::widget> get_widget(size_t index)override;
	private:
		std::vector<std::string> _labels;
	};
public:
	ui(const utki::shared_ref<ruis::context>& c, const tml::forest& desc);
	virtual ~ui(){};

	void set_engine(std::weak_ptr<engine> engine);
	void set_op(game::game_type op);
	void on_game_start(game::game_gen_result gen_res);
	void on_game_decision();

	void update_skin();
	bool close_prize();
	void set_prize_level(game::game_type gt, unsigned int lvl);
#ifndef NO_SCROLL
	void scroll_to_pos(float p);
#endif

private:
	void show_prize(bool show);

private:
	std::shared_ptr<ruis::text> _label_first;
	std::shared_ptr<ruis::text> _label_second;
	std::shared_ptr<ruis::drop_down_box> _opsel;
	std::shared_ptr<ruis::scroll_area> _scroll;
	std::shared_ptr<progress> _progress;
	std::shared_ptr<number_selector> _selector;
	std::shared_ptr<field> _field;
	std::weak_ptr<engine> _eng;
#ifdef NO_SCROLL
	std::shared_ptr<ruis::image_button> _toggle_selector;
#endif
	std::shared_ptr<prize_puzzle> _prize;
	std::shared_ptr<ruis::image_button> _open_puzzle;
};
