#include <ruisapp/application.hpp>

#include <ruis/widgets/button/base/push_button.hpp>
#include <ruis/widgets/button/base/image_button.hpp>
#include <ruis/widgets/proxy/key_proxy.hpp>
#include <ruis/config.hpp>

#include <aumiks/speakers.hpp>
#include <aumiks/wav_sound.hpp>

#include "ui.hpp"
#include "field.hpp"
#include "engine.hpp"
#include "prize_puzzle.hpp"
// #include "game.hpp"


class Application : public ruisapp::application
{
public:
	Application() :
			ruisapp::application("arithmetic-game", ruisapp::window_params(r4::vector2<unsigned>(960, 540)))
	{
	LOG([](auto&o){o << "####>>>> Startup 0" << std::endl;})
#if M_OS_NAME == M_OS_NAME_ANDROID
	  this->set_fullscreen(true);
#endif
		this->gui.init_standard_widgets(*this->get_res_file());
		this->gui.context.get().loader.mount_res_pack(*this->get_res_file("res/main.res"));

		this->gui.context.get().inflater.register_widget<number_selector>("num_selector");
		this->gui.context.get().inflater.register_widget<field>("field");
		this->gui.context.get().inflater.register_widget<ui>("ui");
		this->gui.context.get().inflater.register_widget<progress>("progress");
		this->gui.context.get().inflater.register_widget<prize_puzzle>("prize_puzzle");
#ifdef NO_SCROLL
#define GUI_FILE "res/main.no_scroll.gui"
#else
#define GUI_FILE "res/main.scroll.gui"
#endif
		auto root = this->gui.context.get().inflater.inflate(*this->get_res_file(GUI_FILE));
		this->gui.set_root(root);

		auto ui_ = root.get().try_get_widget_as<ui>("ui");
		_engine = std::make_shared<engine>(storage_dir);
		_engine->init(ui_);
		{
			auto keyProxy = root.get().try_get_widget_as<ruis::key_proxy>("global_key_handler");
			ASSERT(keyProxy)
			  keyProxy->key_handler = [this, ui_](ruis::key_proxy&, const ruis::key_event& e) -> bool{
				if (e.is_down){
					if (e.combo.key == ruis::key::escape
					    && ui_->close_prize()){
						this->quit();
					}
#ifndef NO_SCROLL
					else if (e.combo.key == ruis::key::arrow_left)
					{
						ui_->scroll_to_pos(0.0f);
					}
					else if (e.combo.key == ruis::key::arrow_right)
					{
						ui_->scroll_to_pos(1.0f);
					}
#endif
				}
				return false;
			};
		}
	LOG([](auto&o){o << "####>>>> Startup 13" << std::endl;});
	}

private:
	std::shared_ptr<engine> _engine;
};


ruisapp::application_factory app_fac([](auto executable, auto args){
	std::unique_ptr<Application> res = std::make_unique<Application>();
	return res;
});
