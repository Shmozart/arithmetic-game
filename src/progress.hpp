#pragma once
// #include <ruis/widgets/label/text.hpp>
#include <ruis/widgets/label/image.hpp>

class skin;

class progress : public ruis::container
{
private:
	static const unsigned int resolution = 10;
public:
	progress(const utki::shared_ref<ruis::context>& c, const tml::forest& desc);

	void set_progress(unsigned int progress, std::shared_ptr<skin> skin);

private:
// 	std::shared_ptr<ruis::text> _text;
	std::array<std::shared_ptr<ruis::image>, resolution> _imgs;
};
