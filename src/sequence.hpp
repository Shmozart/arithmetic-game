#pragma once

#include <ruis/config.hpp>
#include <ruis/res/image.hpp>
#include <vector>
#include <memory>
#include "skin_img.hpp"

// time is [0,1]
// typedef ruis::vector2
// (*trajectory)(float time, const ruis::vector2 &start, const ruis::vector2 &end);
typedef std::function<ruis::vector2(float,
				     const ruis::vector2 &,
				     const ruis::vector2 &)> trajectory;

class sequence
{
public:
	sequence(unsigned int track_idx,
		 unsigned int start_p_trj,
		 float start_p_tm,
		 unsigned int end_p_trj,
		 float end_p_tm,
		 unsigned int start,
		 unsigned int end,
		 unsigned int dur);
	ruis::vector2
	get_frame(unsigned int time,
		  const ruis::vector2 &t_pos, const ruis::vector2 &s_pos,
		  std::shared_ptr<const ruis::res::image::texture> &img);
	void add_frame(std::shared_ptr<skin_img> frame);
	bool active(unsigned int t);
	bool unfinished(unsigned int t);

private:
	trajectory _track;
	trajectory _start_p_trj;
	float _start_p_tm;
	trajectory _end_p_trj;
	float _end_p_tm;
	unsigned int _start, _end;
	unsigned int _frame_dur;
	std::vector<std::shared_ptr<skin_img>> _frames;
};
