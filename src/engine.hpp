#pragma once

#include <memory>
#include "game.hpp"

// class game;
class skin;
class ui;
class field;

typedef std::function<void(void)> skin_load_callback_t;

class engine : public std::enable_shared_from_this<engine>
{
public:
	engine(const std::string storage_dir);
	virtual ~engine();
	void init(std::shared_ptr<ui> ui);
	std::shared_ptr<game> get_game();
	std::shared_ptr<skin> get_skin();
	std::shared_ptr<ui> get_ui();
	void load_settings();
private:
	void load_skin(const std::string skin_res);

private:
	std::shared_ptr<game> _game;
	std::shared_ptr<skin> _skin;
	std::shared_ptr<ui> _ui;
	std::function<void(void)> _on_skin_loaded;
	const std::string _storage_dir;
};
