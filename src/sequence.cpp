#include "sequence.hpp"

#define EPS 0.000001f
#define OVER_ZERO_TM(t) ((t) > (-EPS))
#define SUB_ONE_TM(t) ((t) < (1.0f + EPS))
#define ASSERT_TRAJ_TM(t) ASSERT(((t) > (-EPS)) && ((t) < (1.0f + EPS)))

static ruis::vector2
linear_trajectory(float time, const ruis::vector2 &start, const ruis::vector2 &end)
{
	ASSERT(OVER_ZERO_TM(time) && SUB_ONE_TM(time));
	return start*(1.0f-time) + end *time;
}

static ruis::vector2
hump_trajectory(float time, const ruis::vector2 &start, const ruis::vector2 &end)
{
	ASSERT_TRAJ_TM(time);
	ruis::vector2 mid, from, to;
	float t;
	mid = (start + end)*0.5f + ruis::vector2(0.0f, 0.1f);
	if (time < 0.5f)
	{
		from = start;
		to = mid;
		t = time*2.0f;
	}
	else
	{
		from = mid;
		to = end;
		t = time*2.0f - 1.0f;
	}
	return from*(1.0f-t) + to * t;
}

static std::vector<trajectory> tracks =
{
	linear_trajectory,
	hump_trajectory
};

sequence::sequence(unsigned int track,
		   unsigned int start_p_trj,
		   float start_p_tm,
		   unsigned int end_p_trj,
		   float end_p_tm,
		   unsigned int start,
		   unsigned int end,
		   unsigned int dur)
{
	_start_p_trj = tracks[start_p_trj];
	_start_p_tm = start_p_tm;
	_end_p_trj = tracks[end_p_trj];
	_end_p_tm = end_p_tm;
	_track = tracks[track];
	_start = start;
	_end = end;
	_frame_dur = dur;
}

void sequence::add_frame(std::shared_ptr<skin_img> frame)
{
	ASSERT(frame != nullptr)
	_frames.push_back(frame);
}

ruis::vector2
sequence::get_frame(unsigned int time,
		    const ruis::vector2 &t_pos, const ruis::vector2 &s_pos,
		    std::shared_ptr<const ruis::res::image::texture> &img)
{
	ruis::vector2 from, to;
	ASSERT(time <= _end && time >= _start)
	from = _start_p_trj(_start_p_tm, s_pos, t_pos);
	to = _end_p_trj(_end_p_tm, s_pos, t_pos);
	unsigned int idx = ((time - _start)/_frame_dur) % _frames.size();
	img = _frames[idx]->_img;
	return _track((float)(time - _start)/(_end - _start), from, to);
}

bool sequence::active(unsigned int t)
{
	return t >= _start && t <= _end;
}

bool sequence::unfinished(unsigned int t)
{
	return t < _end;
}
