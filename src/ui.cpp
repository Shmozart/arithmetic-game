#include <ruis/widgets/label/text.hpp>
#include <ruis/widgets/button/base/push_button.hpp>
#include <ruis/layouts/pile_layout.hpp>
#include <utki/debug.hpp>

#include "ui.hpp"
#include "field.hpp"
#include "number_selector.hpp"
#include "prize_puzzle.hpp"
#include "skin.hpp"

ui::opsel_model::opsel_model(std::weak_ptr<engine> eng)
{
	for (unsigned i = 0; i < game::e_game_count; ++i)
	{
		_labels.push_back(eng.lock()->get_game()->op_label((game::game_type)i));
	}
}

size_t ui::opsel_model::count() const noexcept
{
	return _labels.size();
}

utki::shared_ref<ruis::widget> ui::opsel_model::get_widget(size_t index)
{
	auto txt = utki::make_shared<ruis::text>(this->get_drop_down_box()->context, tml::forest());
	txt.get().set_text(_labels[index]);
	return txt;
}

ui::ui(const utki::shared_ref<ruis::context>& c, const tml::forest& desc) :
		ruis::widget(std::move(c), desc),
		ruis::container(this->context, desc, ruis::layout::pile)
{
	_label_first = this->try_get_widget_as<ruis::text>("first_term");
	ASSERT(_label_first != nullptr)
	_label_second = this->try_get_widget_as<ruis::text>("second_term");
	ASSERT(_label_second != nullptr)
	_opsel = this->try_get_widget_as<ruis::drop_down_box>("opsel");
	ASSERT(_opsel != nullptr)
	_progress = this->try_get_widget_as<progress>("progress");
	ASSERT(_progress != nullptr)
	_selector = this->try_get_widget_as<number_selector>("number_selector");
	ASSERT(_selector != nullptr)
	_field = this->try_get_widget_as<field>("field");
	ASSERT(_field != nullptr)
	_prize = this->try_get_widget_as<prize_puzzle>("prize_puzzle");
	ASSERT(_prize != nullptr)
#ifdef NO_SCROLL
	_toggle_selector = try_get_widget_as<ruis::image_button>("open_num_selector");
	ASSERT(_toggle_selector != nullptr)

	//NOTE: capture weak reference just in case, to avoid circular references and, thus, memory leaks.
	auto nsWeak = utki::makeWeak(_selector);
	std::dynamic_pointer_cast<ruis::push_button>(_toggle_selector)->clicked =
		[nsWeak](ruis::push_button& b)
		{
			if (auto l = nsWeak.lock()) {
				l->setVisible(true);
			}
		};
#else
	_scroll = this->try_get_widget_as<ruis::scroll_area>("scroll");
	ASSERT(_scroll != nullptr)
#endif
	_open_puzzle = try_get_widget_as<ruis::image_button>("open_puzzle");
	ASSERT(_open_puzzle != nullptr)
	std::dynamic_pointer_cast<ruis::push_button>(_open_puzzle)->click_handler =
		[this](ruis::push_button& b)
		{
			this->show_prize(true);
		};

	_opsel->selection_handler = [this](ruis::selection_box& dds)
		{
			_prize->set_active_puzzle((game::game_type)dds.get_selection());
			this->_eng.lock()->get_game()->start_game((game::game_type)dds.get_selection());
		};
}

void ui::set_engine(std::weak_ptr<engine> engine)
{
	_eng = engine;
	_selector->set_engine(_eng);
	_field->set_engine(_eng);
	_opsel->set_provider(std::make_shared<opsel_model>(_eng));
}

void ui::set_prize_level(game::game_type gt, unsigned int lvl)
{
	std::cout << "gt: " << gt << ", lvl: " << lvl << std::endl;
	_prize->set_prize_level(gt, lvl);
}

void ui::set_op(game::game_type op)
{
	_prize->set_active_puzzle(op);
	_opsel->set_selection(op);
}

void ui::on_game_start(game::game_gen_result gen_res)
{
	_label_first->set_text(std::to_string(gen_res.first));
	_label_second->set_text(std::to_string(gen_res.second));
	_selector->set_mode(gen_res.mode);
	_selector->set_ready_enabled(true);
#ifdef NO_SCROLL
	_field->set_animation(0);
	_toggle_selector->setEnabled(true);
#else
	_field->set_animation(0, _opsel->get_selection() == game::e_subtraction);
	scroll_to_pos(1.0f);
	invalidate_layout();
#endif
	_opsel->set_enabled(true);
	_progress->set_progress(gen_res.progress, _eng.lock()->get_skin());
	_prize->set_prize_level(gen_res.prize_level);
	if (gen_res.show_prize)
	{
		std::cout << "Try to show puzzle" << std::endl;
		show_prize(true);
	}
}

void ui::on_game_decision()
{
	_opsel->set_enabled(false);
	_selector->set_ready_enabled(false);
#ifdef NO_SCROLL
	_toggle_selector->setEnabled(false);
	_field->set_animation(50);
#else
	_field->set_animation(50, false);
	scroll_to_pos(1.0f);
#endif
}

void ui::update_skin()
{
#ifdef NO_SCROLL
	_toggle_selector->set_pressed_image(_eng.lock()->get_skin()->get_vect_img(skin::e_open_num));
	_toggle_selector->set_unpressed_image(_eng.lock()->get_skin()->get_vect_img(skin::e_open_num));
#endif
	_selector->update_skin(_eng.lock()->get_skin());
#ifdef __PUZZLE__
	_prize->update_skin(_eng.lock()->get_skin());
#endif
	_open_puzzle->set_pressed_image(_eng.lock()->get_skin()->get_vect_img(skin::e_open_puzzle));
	_open_puzzle->set_unpressed_image(_eng.lock()->get_skin()->get_vect_img(skin::e_open_puzzle));
}

bool ui::close_prize()
{
	if (_prize->is_visible()) {
		show_prize(false);
		return false;
	}
	return true;
}

void ui::show_prize(bool show)
{
	_prize->set_visible(show);
	_field->set_visible(!show);
#ifdef NO_SCROLL
	_toggle_selector->setEnabled(!show);
#else
	_selector->set_visible(!show);
#endif
}

#ifndef NO_SCROLL
void ui::scroll_to_pos(float p)
{
	auto scrl = utki::make_weak(_scroll);
	this->context.get().run_from_ui_thread([scrl, p](){
			if (auto l = scrl.lock())
				l->set_scroll_factor(ruis::vector2(p, 0.0f));
				});
}
#endif
