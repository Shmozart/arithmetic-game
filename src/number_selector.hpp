#pragma once

#include <ruis/widgets/button/base/button.hpp>
#include <ruis/layouts/linear_layout.hpp>
#include "number_cell.hpp"

class engine;
class skin;

#define SEL_UNSIGNED number_selector::selector_mode(number_selector::e_unsigned, 0)
#define SEL_SIGNED number_selector::selector_mode(number_selector::e_signed, 0)
#define SEL_GRP_SMPL(n) \
	number_selector::selector_mode(number_selector::e_group_simple, (n))
#define SEL_GRP(n) \
	number_selector::selector_mode(number_selector::e_group, (n))

class number_selector : public ruis::container
{
public:
	enum selector_mode_enum
	{
		e_unsigned,
		e_signed,
		e_group_simple,
		e_group
	};

	struct selector_mode
	{
		selector_mode() :
			_mode(e_unsigned),
			_group_size(0u){};
		selector_mode(selector_mode_enum mode, unsigned int grp_sz) :
			_mode(mode),
			_group_size(grp_sz){};
		selector_mode_enum _mode;
		unsigned int _group_size;
	};
public:
	number_selector(const utki::shared_ref<ruis::context>& c, const tml::forest& desc);
	virtual ~number_selector(){};

	void init();
	void set_engine(std::weak_ptr<engine> engine);

	void update_skin(std::shared_ptr<skin> skin);

	void set_mode(selector_mode mode);

	void set_unhide_but(std::shared_ptr<ruis::image_button> b);

	void set_ready_enabled(bool enabled);

private:
	void new_value(int value);
	bool is_selected(unsigned int cell_idx, unsigned int sel_idx);
	int transform_value(int value);
	void clear();

	void update_cell_imgs();
	void cell_images(unsigned int cell_idx,
			 std::shared_ptr<const ruis::res::image> &pressed,
			 std::shared_ptr<const ruis::res::image> &unpressed,
			 std::string &cell_txt);

private:
	std::weak_ptr<engine> _eng;
	virtual unsigned int value2index(int value);
	virtual int index2value (unsigned int index);
	std::array<std::shared_ptr<number_cell>, 100> _cells;

	std::shared_ptr<ruis::container> _numbersTable;

	int _selected_number;
	selector_mode _mode;
};
