#pragma once

#include "game_addition.hpp"

class game_subtraction : public game_addition
{
public:
	virtual ~game_subtraction();
	game_subtraction();

	virtual void default_init() override;
	virtual number_selector::selector_mode layout_type() override;

	virtual ruis::vector2 target_position(unsigned int idx,
					       game::stage stage,
					       bool &annihilation) override;

	virtual ruis::vector2 pre_shot_position(unsigned int idx) override;
	virtual void target_count(unsigned int &n_targets, unsigned int &n_shots) override;
	virtual void advance_stage() override;
	virtual unsigned int required_right_answers() override;
	virtual void terms_labels(unsigned int &first, unsigned int &second) override;
	// virtual game::game_gen_result final_gen_res() override;
	virtual unsigned int get_prize_level() override;
	virtual bool stage_is_final() override;

private:
	const static unsigned int POSITIVE_SELECTOR = 0x1;
	const static unsigned int POSITIVE_RESULT = 0x2;
	size_t _preshot_count;
	size_t _target_count;
	unsigned int _positive_flags;
};
