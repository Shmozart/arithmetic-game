#pragma once

#include <ruis/widgets/widget.hpp>
#include <ruis/updateable.hpp>
#include "engine.hpp"

class field :
	public ruis::widget,
	public ruis::updateable
{
public:
	field(const utki::shared_ref<ruis::context>& c, const tml::forest& desc);

	void set_engine(std::weak_ptr<engine> engine);

	virtual void render(const ruis::matrix4& matrix) const override;

#ifdef NO_SCROLL
	void set_animation(unsigned int period);
#else
	void set_animation(unsigned int period, bool have_preshots);
#endif

	void update(std::uint32_t dtMs) override;

#ifndef NO_SCROLL
	virtual ruis::vector2 measure(const ruis::vector2& quotum) const override;
#endif
private:
	ruis::vector2 img_bl(const game::game_object &obj) const;

private:
	std::weak_ptr<engine> _eng;
	bool _in_animation;
#ifndef NO_SCROLL
	bool _with_shots;
#endif
};
