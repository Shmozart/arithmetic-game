#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "game_addition.hpp"

game_addition::~game_addition(){}
game_addition::game_addition()
{
	default_init();
}

void game_addition::target_count(unsigned int &n_targets, unsigned int &n_shots)
{
	n_shots = 0;
	n_targets = _new_layout.first + _new_layout.second;
}

void game_addition::default_init()
{
	game_layout::default_init();
	_gen_res.prize_level = e_explain_under_ten;
	_right_answers = 0;
	_prev_layout(1, 0);
}

void game_addition::select_terms()
{
	unsigned int a,b,c,d;

	_new_layout = _prev_layout;
	switch (_gen_res.prize_level)
	{
	case e_explain_under_ten:
		if (_new_layout.first == 9) {
			advance_stage();
			select_terms();
		}
		else if (_new_layout.first + _new_layout.second == 10) {
			++_new_layout.first;
			_new_layout.second = 1;
		}
		else
			++_new_layout.second;
		break;
	case e_quiz_under_ten:
		select_rnd_unipairs(9, _new_layout.first, _new_layout.second);
		++_new_layout.first;
		++_new_layout.second;
		break;
	case e_explain_over_ten:
		if (_new_layout.first == 10 && _new_layout.second == 10) {
			advance_stage();
			select_terms();
		}
		else if (_new_layout.second == 10) {
			++_new_layout.first;
			_new_layout.second = 11 - _new_layout.first;
		}
		else
			++_new_layout.second;
		break;
	case e_quiz_over_ten:
		select_rnd_unipairs(9, _new_layout.first, _new_layout.second);
		_new_layout.first = 10 - _new_layout.first;
		_new_layout.second = 10 - _new_layout.second;
		break;
	case e_quiz_no_tens:
		_new_layout(rand()%10 + 1, rand()%10 + 1);
		break;
	case e_tens_explain_under_ten:
		a = _new_layout.first/10;
		b = _new_layout.second/10;
		if (a == 8) {
			advance_stage();
			select_terms();
		}
		else if (a + b == 9) {
			++a;
			b = 1;
		}
		else
			++b;
		if (a == 1)
			c = d = 0;
		else if ((a < 6 && b == 1) || a > 5) {
			select_rnd_unisum(10, c, d);
		}
		else
		{
			c = _new_layout.first % 10;
			d = _new_layout.second % 10;
		}
		_new_layout(10*a + c, 10*b + d);
		break;
	case e_tens_quiz_under_ten:
		select_rnd_unipairs(8, a, b);
		++a;
		++b;
		select_rnd_unipairs(10, c, d);
		_new_layout(10*a + c, 10*b + d);
		break;
	case e_tens_explain_over_ten:
		a = _new_layout.first/10;
		b = _new_layout.second/10;
		if (a == 8) {
			advance_stage();
			select_terms();
		}
		else if (a + b == 9) {
			++a;
			b = 1;
		}
		else
			++b;
		if (a == 1)
			c = d = 0;
		else if ((a < 6 && b == 1) || a > 5) {
			select_rnd_unisum(9, c, d);
			c = 9 - c;
			d = 9 - d;
		}
		else
		{
			c = _new_layout.first % 10;
			d = _new_layout.second % 10;
		}
		_new_layout(10*a + c, 10*b + d);
		break;
	case e_tens_quiz_over_ten:
		select_rnd_unipairs(7, a, b);
		++a;
		++b;
		select_rnd_unipairs(9, c, d);
		c = 9 - c;
		d = 9 - d;
		_new_layout(10*a + c, 10*b + d);
		break;
	case e_quiz_with_tens:
	default:
		select_rnd_unipairs(99, _new_layout.first, _new_layout.second);
		++_new_layout.first;
		++_new_layout.second;
		break;
	}
}

void game_addition::advance_stage()
{
	if (_gen_res.prize_level == e_stage_count)
		return;
	_select_term_counter = 0;
	++_gen_res.prize_level;
	_gen_res.show_prize = true;
	_right_answers = 0;
	switch (_gen_res.prize_level) {
	case e_explain_under_ten:
		_prev_layout(1, 0);
		break;
	case e_explain_over_ten:
		_prev_layout(1, 9);
		break;
	case e_tens_explain_under_ten:
	case e_tens_explain_over_ten:
		_prev_layout(10, 0);
		break;
	default:
		break;
	}
}

/* Simplification priorities:
 * Drop lower digit (make it 0).
 * Reduce lower digit to 1.
 * Reduce higher digit to 1.
 * If same priority action is available in both components,
 * one with smaller affected digit is selected.
 * If Higher digit is missing, lower cannot be dropped
 * If digit is already 1 it cannot be reduced
 */
void game_addition::simplify_layout()
{
	unsigned int a,b,c,d;
	a = _new_layout.first/10;
	b = _new_layout.first%10;
	c = _new_layout.second/10;
	d = _new_layout.second%10;
	ASSERT((a != 0 || b != 0) && (c != 0 || d !=0))
	if (a*b*c*d > 0) {
		//Both lower digits can be dropped, select smaller one.
		if (b < d)
			b = 0;
		else
			d = 0;
	}
	else if (a*b > 0) //Can drop lower digit in first.
		b = 0;
	else if (c*d > 0) //Can drop lower digit in second.
		d = 0;
	else if (b > 1 && d > 1) {
		//Can reduce both lower digits to 1, select smaller one.
		if (b < d)
			b = 1;
		else
			d = 1;
	}
	else if (b > 1)
		b = 1;
	else if (d > 1)
		d = 1;
	else if (a > 1 && c > 1) {
		//Can reduce both higher digits to 1, select smaller one.
		if (a < c)
			a = 1;
		else
			c = 1;
	}
	else if (a > 1)
		a = 1;
	else if (c > 1)
		c = 1;

	_new_layout(a*10+b, c*10+d);
}

void game_addition::advance_to_failed()
{
	unsigned int a,b,c,d;
	unsigned int af,bf,df;
	a = _new_layout.first/10;
	c = _new_layout.first%10;
	b = _new_layout.second/10;
	d = _new_layout.second%10;
	af = _failed_layout.first/10;
	bf = _failed_layout.second/10;
	df = _failed_layout.second%10;

	if (a == af && b == bf && d == df)
		++c;
	else if (a == af && b == bf)
		++d;
	else if (b == bf)
		++a;
	else
		++b;

	_new_layout(a*10+c, b*10+d);
}

bool game_addition::stage_is_quiz()
{
	switch (_gen_res.prize_level)
	{
	case e_quiz_under_ten:
	case e_quiz_over_ten:
	case e_quiz_no_tens:
	case e_tens_quiz_under_ten:
	case e_tens_quiz_over_ten:
	case e_quiz_with_tens:
	case e_stage_count:
		return true;
	default:
		return false;
	}
}

bool game_addition::stage_is_final()
{
	return _gen_res.prize_level == e_quiz_with_tens;
}

unsigned int game_addition::required_right_answers()
{
	switch (_gen_res.prize_level)
	{
	case e_explain_under_ten:
		return 45;
	case e_explain_over_ten:
		return 55;
	case e_tens_explain_under_ten:
	case e_tens_explain_over_ten:
		return 36;
	case e_quiz_with_tens:
		return 25;
	default:
		return game_layout::required_right_answers();
	}
}

ruis::vector2 game_addition::target_position(unsigned int idx,
					      game::stage stage,
					      bool &annihilation)
{
	annihilation = false;
	ruis::vector2 p;
	if (idx < _new_layout.first)
		p = target_position_base(idx);
	else
	{
		p = target_position_base(idx - _new_layout.first);
		p.y() += game_layout::targ_grid_size * (_new_layout.first/10 +
							(_new_layout.first%10 > 0?1.25:0.25));
	}
	return p;
}
