#pragma once

#include <ruis/widgets/label/text.hpp>
#include "game.hpp"

class prize_puzzle : public ruis::container
{
public:
	prize_puzzle(const utki::shared_ref<ruis::context>& ctx, const tml::forest& desc);

	void set_prize_level(unsigned int lvl);
	void set_prize_level(game::game_type, unsigned int lvl);
	void set_active_puzzle(game::game_type gt);
#ifdef __PUZZLE__
	void update_skin(std::shared_ptr<skin> skin);
#endif
private:
#ifdef __PUZZLE__
	std::array<std::shared_ptr<ruis::pile>, game::e_game_count> _puzzles;
#else
	std::shared_ptr<ruis::text> _level_info;
	std::array<std::pair<unsigned int, unsigned int>, game::e_game_count> _levels;
#endif
	game::game_type _active_puzzle;
};
