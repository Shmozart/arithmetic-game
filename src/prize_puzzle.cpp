#include "prize_puzzle.hpp"
#include "game_addition.hpp"
#include "game_multiplication.hpp"
#include "game_division.hpp"
#include "skin.hpp"

#include <ruis/layouts/pile_layout.hpp>

prize_puzzle::prize_puzzle(const utki::shared_ref<ruis::context>& ctx, const tml::forest& desc) :
		widget(std::move(ctx), desc),
		container(this->context, desc, ruis::layout::pile)
{
#ifdef __PUZZLE__
	size_t i = 0;
	for (auto && c : children())
	{
		_puzzles[i] = std::dynamic_pointer_cast<ruis::pile>(c);
		++i;
	}
#else
	_level_info = utki::dynamic_reference_cast<ruis::text>(children()[0]).to_shared_ptr();
	_levels[game::e_addition].first = 0;
	_levels[game::e_addition].second = game_addition::e_stage_count;
	_levels[game::e_subtraction].first = 0;
	_levels[game::e_subtraction].second = game_addition::e_stage_count*2;
	_levels[game::e_multiplication].first = 0;
	_levels[game::e_multiplication].second = game_multiplication::e_stage_count;
	_levels[game::e_division].first = 0;
	_levels[game::e_division].second = game_multiplication::e_stage_count;
#endif
}

void prize_puzzle::set_prize_level(unsigned int lvl)
{
	set_prize_level(_active_puzzle, lvl);
}

void prize_puzzle::set_prize_level(game::game_type gt, unsigned int lvl)
{
#ifdef __PUZZLE__
	unsigned int i = 0;
	for (auto &&iter: _puzzles[gt]->children())
	{
		iter->set_visible(i <= lvl);
		++i;
	}
#else
	_levels[gt].first = lvl;
	if (_active_puzzle == gt)
		set_active_puzzle(gt);//Looks like non-sense, update text.
#endif
}

void prize_puzzle::set_active_puzzle(game::game_type gt)
{
#ifdef __PUZZLE__
	unsigned int i;
	for (i = 0; i < game::e_game_count; ++i)
	{
		_puzzles[i]->set_visible(i == gt);
	}
#else
	_level_info->set_text(std::to_string(_levels[gt].first) +
			      std::string(" / ") +
			      std::to_string(_levels[gt].second));
#endif
	_active_puzzle = gt;
}

#ifdef __PUZZLE__
void prize_puzzle::update_skin(std::shared_ptr<skin> skin)
{
	unsigned int i;
	for (i = 0; i < game::e_game_count; ++i)
	{
		size_t j = 0;
		for (auto &&img: _puzzles[i]->children())
		{
			std::dynamic_pointer_cast<ruis::image>(img)->
				set_image(skin->get_puzzle_image((game::game_type)i, j));
			++j;
		}
	}
}
#endif
