#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "game_division.hpp"

game_division::~game_division(){}
game_division::game_division()
{
	default_init();
}

void game_division::default_init()
{
	game_layout::default_init();
	_gen_res.prize_level = e_no_remainder_2;
	_prev_layout(0, 2);
}

number_selector::selector_mode game_division::layout_type()
{
	return SEL_GRP_SMPL(_new_layout.second);
}

void game_division::target_count(unsigned int &n_targets, unsigned int &n_shots)
{
	n_shots = 0;
	n_targets = _new_layout.first;
}

ruis::vector2 game_division::target_position(unsigned int idx,
					      game::stage stage,
					      bool &annihilation)
{
	annihilation = false;
	return target_position_base(idx);
}

unsigned int game_division::fixed_term()
{
	switch (_gen_res.prize_level)
	{
	case e_no_remainder_2:
	case e_remainder_2:
	case e_quiz_2:
		return 2;
		break;
	case e_no_remainder_3:
	case e_remainder_3:
	case e_quiz_3:
		return 3;
		break;
	case e_no_remainder_4:
	case e_remainder_4:
	case e_quiz_4:
		return 4;
		break;
	case e_no_remainder_5:
	case e_remainder_5:
	case e_quiz_5:
		return 5;
		break;
	case e_no_remainder_6:
	case e_remainder_6:
	case e_quiz_6:
		return 6;
		break;
	case e_no_remainder_7:
	case e_remainder_7:
	case e_quiz_7:
		return 7;
		break;
	case e_no_remainder_8:
	case e_remainder_8:
	case e_quiz_8:
		return 8;
		break;
	case e_no_remainder_9:
	case e_remainder_9:
	case e_quiz_9:
		return 9;
		break;
	default:
		return 2;
	}
}

bool game_division::has_remainder()
{
	switch (_gen_res.prize_level)
	{
	case e_remainder_2:
	case e_remainder_3:
	case e_remainder_4:
	case e_remainder_5:
	case e_remainder_6:
	case e_remainder_7:
	case e_remainder_8:
	case e_remainder_9:
		return true;
	default:
		return false;
	}
}

void game_division::select_terms()
{
	_new_layout = _prev_layout;
	unsigned int fixed = fixed_term();
	if (_gen_res.prize_level == e_final_quiz) {
		_new_layout.second = (rand()%9 + 2);
		_new_layout.first = (rand()%(_new_layout.second*10) + 1);
		return;
	}
	if (stage_is_quiz()) {
		_new_layout.first = rand()%(fixed*10 - 1) + 2;
		_new_layout.second = fixed;
		return;
	}
	if (_new_layout.first <= fixed * 9) {
		_new_layout.first += fixed;
		if (has_remainder()) {
			if (_new_layout.first % fixed > 0)
				_new_layout.first += fixed - _new_layout.first % fixed;
			_new_layout.first -= rand()%(fixed - 1) + 1;
		}
		return;
	}
	advance_stage();
	select_terms();
}

void game_division::advance_stage()
{
	if (_gen_res.prize_level == e_stage_count)
		return;
	++_gen_res.prize_level;
	_gen_res.show_prize = true;
	_select_term_counter = 0;
	_right_answers = 0;
	if (!stage_is_quiz())
		_prev_layout(0, fixed_term());
}

void game_division::simplify_layout()
{
	unsigned int remainder =
		_new_layout.first % _new_layout.second;
	if (remainder > 0) {
		if (_new_layout.first == remainder)
			_new_layout.first = 1;
		else
			_new_layout.first -= remainder;
	}
	else
		_new_layout.first = _new_layout.second;
}

void game_division::advance_to_failed()
{
	ASSERT(_new_layout.second == _failed_layout.second)
	ASSERT(_new_layout.first < _failed_layout.first)
	if (_new_layout.first < _failed_layout.first/_failed_layout.second)
		_new_layout.first += _new_layout.second;
	else
		++_new_layout.first;
}

bool game_division::stage_is_quiz()
{
	switch (_gen_res.prize_level)
	{
	case e_quiz_2:
	case e_quiz_3:
	case e_quiz_4:
	case e_quiz_5:
	case e_quiz_6:
	case e_quiz_7:
	case e_quiz_8:
	case e_quiz_9:
	case e_final_quiz:
	case e_stage_count:
		return true;
	default:
		return false;
	}
}

bool game_division::stage_is_final()
{
	return _gen_res.prize_level == e_final_quiz;
}

unsigned int game_division::required_right_answers()
{
	if (stage_is_final())
		return 25;
	if (stage_is_quiz())
		return game_layout::required_right_answers();
	return 10;
}
