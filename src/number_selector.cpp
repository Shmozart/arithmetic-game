#include <ruis/widgets/label/text.hpp>
#include <ruis/gui.hpp>
#include <ruis/widgets/button/image_push_button.hpp>
#include <ruis/layouts/linear_layout.hpp>
#include <ruis/layouts/pile_layout.hpp>

#include "number_selector.hpp"
#include "engine.hpp"
#include "skin.hpp"

namespace{
const auto numSelectorGui_c = tml::read(R"qwertyuiop(
	@color{ visible{true} lp{dx {fill} dy{fill}} color{0xffc0b040}}
	@column
	{
		visible{true}
		lp{dx {max} dy{fill}}
		@column
		{
			id { numbers_table }
			lp
			{
				dx { min }
				dy { fill }
				weight { 10 }
			}
		}
		@row
		{
			lp
			{
				dx { fill }
				dy { fill }
				weight { 1 }
			}
			@widget
			{
				lp
				{
					dx { fill }
					weight { 1 }
				}
			}
			@image_push_button
			{
				id { hide_button }
				keep_aspect_ratio { true }
				lp
				{
					dy { fill }
				}
			}
			@image_push_button
			{
				id { ready_button }
				keep_aspect_ratio { true }
				lp
				{
					dy { fill }
				}
			}
		}
	}
	)qwertyuiop");
}

number_selector::number_selector(const utki::shared_ref<ruis::context>& c, const tml::forest& desc) :
		widget(std::move(c), desc),
		container(this->context, numSelectorGui_c, ruis::layout::pile)
{
	this->_numbersTable = this->try_get_widget_as<ruis::container>("numbers_table");
	ASSERT(this->_numbersTable)
#ifdef NO_SCROLL
	{
		auto b = this->try_get_widget_as<ruis::push_button>("hide_button");
		ASSERT(b)
		b->clicked = [this](ruis::push_button &b)
			{
				this->setVisible(false);
			};
	}
#endif
	{
		auto b = this->try_get_widget_as<ruis::push_button>("ready_button");
		ASSERT(b)
		b->click_handler = [this](ruis::push_button &b)
			{
#ifdef NO_SCROLL
				this->setVisible(false);
#endif
				_eng.lock()->get_game()->make_decision(this->_selected_number);
				this->clear();
			};
	}

	this->init();
	// set_visibility(e_hidden);
}

void number_selector::set_engine(std::weak_ptr<engine> engine)
{
	_eng = engine;
}

void number_selector::init()
{
	unsigned int width = (_mode._mode == e_group_simple)?_mode._group_size:10;
	this->_numbersTable->clear();

	_selected_number = 0;
	std::shared_ptr<ruis::container> row;
	for (unsigned int i = 0; i < 100; ++i)
	{
		if (i%10 == 0)
			row = this->context.get().inflater.inflate_as<ruis::container>(R"qwertyuiop(
					@row
					{
						lp
						{
							dy { fill }
							dx { max }
							weight { 1 }
						}
					}
				)qwertyuiop").to_shared_ptr();
		ASSERT(row)

		_cells[i] = utki::make_shared<number_cell>(this->context, index2value(i)).to_shared_ptr();
		_cells[i]->_button->pressed_change_handler = [this, i](ruis::button& b)
		{
			this->new_value(this->_cells[i]->value());
		};

		if (i%10 < width){
			auto p = _cells[i];
			ASSERT(p)
			row->push_back(utki::shared_ref<number_cell>(std::move(p)));
		}

		if (i%10 == 9) {
			auto p = row;
			this->_numbersTable->push_back(utki::shared_ref<ruis::container>(std::move(p)));
		}
	}
}

void number_selector::set_mode(selector_mode mode)
{
	// setVisible(true);
	unsigned int divs = 0;
	if (mode._mode == e_group_simple) ++divs;
	if (_mode._mode == e_group_simple) ++divs;
	bool reinit = ((divs == 1) ||
		       (divs == 2 && _mode._group_size != mode._group_size));
	_mode = mode;
	if (reinit) {
		init();
	}
	update_cell_imgs();
}

void number_selector::cell_images(unsigned int cell_idx,
				  std::shared_ptr<const ruis::res::image> &pressed,
				  std::shared_ptr<const ruis::res::image> &unpressed,
				  std::string &cell_txt)
{
	skin::images p = skin::e_selected_cell,u = skin::e_empty_cell;
	cell_txt.clear();
	switch(_mode._mode)
	{
	case e_group_simple:
		if ((cell_idx % 10) < _mode._group_size) {
			cell_txt += std::to_string((cell_idx + 11 - _mode._group_size)/10);
			if ((cell_idx % 10) < _mode._group_size - 1)
				cell_txt += " (" + std::to_string((cell_idx % 10) + 1) + ")";
		}
		break;
	case e_group:
		// break; temporarily fall through, while not implemented
	case e_unsigned:
		cell_txt += std::to_string(cell_idx + 1);
		break;
	case e_signed:
		if (cell_idx < 50)
		{
			p = skin::e_neg_selected_cell;
			u = skin::e_neg_empty_cell;
		}
		cell_txt += std::to_string(transform_value(cell_idx + 1));
		break;
	default:
		break;
	}
	pressed = _eng.lock()->get_skin()->get_vect_img(p);
	unpressed = _eng.lock()->get_skin()->get_vect_img(u);
}

void number_selector::update_cell_imgs()
{
	std::shared_ptr<const ruis::res::image> pressed;
	std::shared_ptr<const ruis::res::image> unpressed;
	std::string cell_txt;
	for (unsigned int i = 0; i < 100; ++i) {
		cell_images(i, pressed, unpressed, cell_txt);
		_cells[i]->_button->set_pressed_image(pressed);
		_cells[i]->_button->set_unpressed_image(unpressed);
		_cells[i]->_label->set_text(cell_txt);
	}
}

void number_selector::update_skin(std::shared_ptr<skin> skin)
{
	update_cell_imgs();
	{
		auto b = this->try_get_widget_as<ruis::image_push_button>("ready_button");
		b->set_pressed_image(skin->get_vect_img(skin::e_ready));
		b->set_unpressed_image(skin->get_vect_img(skin::e_ready));
	}
#ifdef NO_SCROLL
	{
		auto b = this->try_get_widget_as<ruis::image_push_button>("hide_button");
		b->set_pressed_image(skin->get_vect_img(skin::e_hide_numsel));
		b->set_unpressed_image(skin->get_vect_img(skin::e_hide_numsel));
	}
#endif
}

int number_selector::index2value(unsigned int index)
{
	return (int)index + 1;
}

unsigned int number_selector::value2index(int value)
{
	ASSERT(value > 0)
	return (unsigned int)value - 1;
}

void number_selector::new_value(int value)
{
	unsigned int selected_index = value2index(value);
	for (unsigned int i = 0; i < 100; ++i)
		_cells[i]->set_selected(is_selected(i, selected_index));

	_selected_number = transform_value(value);
}

bool number_selector::is_selected(unsigned int cell_idx, unsigned int sel_idx)
{
	bool res = false;
	switch (_mode._mode)
	{
	case e_group_simple:
	case e_unsigned:
		res = cell_idx <= sel_idx;
		break;
	case e_signed:
		res = (sel_idx < 50)?
			((cell_idx < 50) &&
			 ((cell_idx/10 > sel_idx/10) ||
			  ((cell_idx/10 == sel_idx/10) &&
			   (cell_idx %10 <= sel_idx%10)))):
			((cell_idx <= sel_idx)&&(cell_idx >= 50));
		break;
	case e_group:
		break;
	default:
		break;
	}
	return res;
}

int number_selector::transform_value(int value)
{
	int res = value;
	switch (_mode._mode)
	{
	case e_unsigned:
		break;
	case e_signed:
		if (value > 50)
			res = value - 50;
		else
			res = (value/10 - 4)*10 - ((value%10)?value%10:20);
		break;
	case e_group_simple:
		res = value/10*_mode._group_size + std::min((int)_mode._group_size, (value %10));
		break;
	case e_group:
		break;
	default:
		break;
	}
	return res;
}

void number_selector::clear()
{
	for (unsigned int i = 0; i < 100; ++i)
		_cells[i]->set_selected(false);

	_selected_number = 0;
}

void number_selector::set_ready_enabled(bool enabled)
{
	auto b = this->try_get_widget_as<ruis::image_push_button>("ready_button");
	b->set_enabled(enabled);
}
