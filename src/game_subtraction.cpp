#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "game_subtraction.hpp"

game_subtraction::~game_subtraction(){}
game_subtraction::game_subtraction()
{
	default_init();
	_state.push_back((int *)&_positive_flags);
}

void game_subtraction::default_init()
{
	game_addition::default_init();
	_positive_flags = POSITIVE_SELECTOR | POSITIVE_RESULT;
}

number_selector::selector_mode game_subtraction::layout_type()
{
	return (_positive_flags & POSITIVE_SELECTOR)? SEL_UNSIGNED: SEL_SIGNED;
}

void game_subtraction::target_count(unsigned int &n_targets, unsigned int &n_shots)
{
	if (!_correcting_error)
	{
		if ((_positive_flags & POSITIVE_SELECTOR) ||
		    (_gen_res.prize_level == e_quiz_with_tens && rand()%2 == 0))
			_positive_flags |= POSITIVE_RESULT;
		else
			_positive_flags &= ~POSITIVE_RESULT;
	}


	if (!(_positive_flags & POSITIVE_SELECTOR) && _new_layout.second > 50)
		std::swap(_new_layout.first, _new_layout.second);
	if (_positive_flags & POSITIVE_RESULT) {
		n_targets = _new_layout.first + _new_layout.second;
		n_shots = _new_layout.first;
	}
	else {
		n_shots = _new_layout.first + _new_layout.second;
		n_targets = _new_layout.first;
	}
	_preshot_count = n_shots;
	_target_count = n_targets;
}

void game_subtraction::terms_labels(unsigned int &first, unsigned int &second)
{
	first = _target_count;
	second = _preshot_count;
}

unsigned int game_subtraction::get_prize_level()
{
	return game_layout::get_prize_level() +
		((_positive_flags & POSITIVE_SELECTOR)?0:e_stage_count);
}

void game_subtraction::advance_stage()
{
	if ((_positive_flags & POSITIVE_SELECTOR) && _gen_res.prize_level == e_quiz_with_tens)
	{
		_gen_res.prize_level = e_explain_under_ten;
		_positive_flags = 0;
		_gen_res.show_prize = true;
	}
	else
		game_addition::advance_stage();
}

unsigned int game_subtraction::required_right_answers()
{
	if (_gen_res.prize_level == e_quiz_with_tens)
		return (_positive_flags & POSITIVE_SELECTOR)? 15 : 25;
	return game_addition::required_right_answers();
}

bool game_subtraction::stage_is_final()
{
	return game_addition::stage_is_final() &&
		_positive_flags & POSITIVE_SELECTOR;
}

ruis::vector2 game_subtraction::target_position(unsigned int idx,
						 game::stage stage,
						 bool &annihilation)
{
	if (stage != game::e_shooting || idx < _target_count) {
		annihilation = false;
		return target_position_base(idx);
	}
	annihilation = true;
	return shot_position(idx - _target_count);
}

ruis::vector2 game_subtraction::pre_shot_position(unsigned int idx)
{
	float y_shift = (0.9f - shot_grid_size*(_preshot_count/10+((_preshot_count%10)?1:0)));
	ruis::vector2 p =
		target_position_base(idx, shot_grid_size);
	p.x() += shot_grid_shift;
	p.y() += y_shift;
	return p;
}
