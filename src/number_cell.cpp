#include "number_cell.hpp"

#include <ruis/layouts/pile_layout.hpp>

namespace{
const auto imageToggleDesc_c = tml::read(R"qwertyuiop(

	lp
	{
		dy { fill }
		dx { max }
		weight { 1 }
	}
	@image_toggle
	{
		lp
		{
			dy { fill }
			dx { max }
		}
		keep_aspect_ratio { true }
		id {button}
	}
	@margins{
		lp
		{
			dy { fill}
			dx { fill }
		}
		right{5dp}
		bottom{3dp}
		@bottom{
			lp
			{
				dy { fill}
				dx { fill }
			}
			@right
			{
				lp
				{
					dy { min}
					dx { fill }
				}
				@text
				{
					lp
					{
						dy { min }
						dx { min }
					}
					id {label}
				}
			}
		}
	}

)qwertyuiop");
}

number_cell::number_cell(const utki::shared_ref<ruis::context>& c, int value) :
		widget(std::move(c), imageToggleDesc_c),
		container(this->context, imageToggleDesc_c, ruis::layout::pile)
{

	_button = try_get_widget_as<ruis::image_toggle>("button");
	ASSERT(_button != nullptr)
	_label = try_get_widget_as<ruis::text>("label");
	ASSERT(_label != nullptr)
	*this = value;
	set_selected(false);
}

number_cell::~number_cell()
{
}

void number_cell::set_selected(bool selected)
{
	auto tmp = std::move(_button->pressed_change_handler);
	_button->pressed_change_handler = nullptr;
	ASSERT(!_button->press_handler)
	_button->set_pressed(selected);
	_button->pressed_change_handler = std::move(tmp);
}

void number_cell::operator=(int value)
{
	_value = value;
	_label->set_text(std::to_string(_value));
}
