#pragma once

#include <ruis/res/image.hpp>
#include <aumiks/speakers.hpp>
#include <aumiks/wav_sound.hpp>
#include "sequence.hpp"
#include "game.hpp"
#include "skin_img.hpp"

class skin : public ruis::resource
{
	struct lazy_img
	{
		explicit lazy_img(const papki::file &f,
				  const std::string p)
		{
			_img = nullptr;
			_fi = f.spawn();
			_fi->set_path(p);
		}
		std::unique_ptr<const papki::file> _fi;
		std::shared_ptr<ruis::res::image> _img;
	};
public:
	enum images
	{
		e_background,
		e_target,
		e_cover,
		e_shot,
		e_anti_shot,
		e_open_num,
		e_empty_cell,
		e_neg_empty_cell,
		e_selected_cell,
		e_neg_selected_cell,
		e_cell_unused,
		e_hide_numsel,
		e_ready,
		e_open_puzzle,
		e_progress_on,
		e_progress_off,
		e_img_count
	};
	enum sounds
	{
		e_snd_deciding,
		e_snd_anim,
		e_snd_success,
		e_snd_fail,
		e_snd_prize,
		e_snd_count
	};
	skin(const utki::shared_ref<ruis::context>& c);
	~skin(){};
	std::shared_ptr<const ruis::res::image::texture> get_scaled_img(images img_id);
	std::shared_ptr<ruis::res::image> get_vect_img(images img_id);
#ifdef __PUZZLE__
	std::shared_ptr<ruis::res::image> get_puzzle_image(game::game_type gt, unsigned int idx);
#endif

	ruis::vector2
	get_shooting_obj(bool annihilation, unsigned int &obj_idx,
			 const ruis::vector2 &target_start,
			 const ruis::vector2 &shot_start, unsigned int time,
			 std::shared_ptr<const ruis::res::image::texture> &img);

	ruis::vector2
	get_lon_shot_obj(unsigned int &obj_idx,
			 const ruis::vector2 &shot_pos, unsigned int time,
			 std::shared_ptr<const ruis::res::image::texture> &img);

	ruis::vector2
	get_lon_targ_obj(bool annihilation, unsigned int &obj_idx,
			 const ruis::vector2 &target_pos, unsigned int time,
			 std::shared_ptr<const ruis::res::image::texture> &img);

	void set_screen_dim(ruis::vector2 dim);

	bool shooting_animations_over(unsigned int tm);

	void start_playing(sounds snd, bool loop = false);

private:
	friend class ruis::resource_loader;
	static void load_frames(std::shared_ptr<skin> skin,
				const tml::forest& desc, const papki::file &fi);
	static void load_seq(std::vector<sequence> &seq,
			     std::shared_ptr<skin> skin,
			     const tml::forest& desc, const papki::file &fi,
			     const std::string anim_name);
#ifdef __PUZZLE__
	static std::vector<lazy_img>
	load_puzzle(const tml::tree& desc, const papki::file &fi);
#endif
	static utki::shared_ref<skin> load(const utki::shared_ref<ruis::context>& ctx, const tml::forest& desc, const papki::file &fi);
	void load_sounds();
	std::shared_ptr<skin_img> frame_by_name(std::string name);

	ruis::vector2
	get_obj(unsigned int &obj_idx, std::vector<sequence> &animations,
		const ruis::vector2 &target_start, const ruis::vector2 &shot_start,
		unsigned int time, std::shared_ptr<const ruis::res::image::texture> &img);

private:
	struct scale_info
	{
		scale_info(images idx, float scale_factor) : _idx(idx), _scale_factor(scale_factor) {};
		images _idx;
		std::shared_ptr<const ruis::res::image::texture> _scaled_img;
		float _scale_factor;
	};
	std::vector<std::shared_ptr<skin_img>> _anim_frames;
	std::vector<sequence> _shooting_animations;
	std::vector<sequence> _annihilation_animations;
	std::vector<sequence> _lonely_shot_animations;
	std::vector<sequence> _lonely_targ_animations;
	std::array<std::shared_ptr<ruis::res::image>, e_img_count> _vect_imgs;
#ifdef __PUZZLE__
	std::vector<std::vector<lazy_img>> _puzzle_imgs;
#endif
	std::vector<scale_info> _scaled_imgs;
	std::array<std::shared_ptr<aumiks::wav_sound>, e_snd_count> _sounds;
	ruis::vector2 cur_dim = -1;
	aumiks::speakers _sink;
};
