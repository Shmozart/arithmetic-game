#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include "game_multiplication.hpp"

game_multiplication::~game_multiplication(){}
game_multiplication::game_multiplication()
{
	default_init();
}

void game_multiplication::default_init()
{
	game_layout::default_init();
	_gen_res.prize_level = e_explain_2;
	_prev_layout(1, 2);
}

void game_multiplication::target_count(unsigned int &n_targets, unsigned int &n_shots)
{
	n_targets = _new_layout.first * _new_layout.second;
	n_shots = 0;
}

ruis::vector2 game_multiplication::target_position(unsigned int idx,
						    game::stage stage,
						    bool &annihilation)
{
	unsigned int tens, ones;
	annihilation = false;
	tens = idx/_new_layout.first;
	ones = idx %_new_layout.first + 10-_new_layout.first;
	return target_position_base(tens*10+ones);
}

bool game_multiplication::stage_is_final()
{
	return _gen_res.prize_level == e_final_quiz;
}

unsigned int game_multiplication::fixed_term()
{
	switch (_gen_res.prize_level)
	{
	case e_explain_2:
	case e_quiz_2:
		return 2;
		break;
	case e_explain_3:
	case e_quiz_3:
		return 3;
		break;
	case e_explain_4:
	case e_quiz_4:
		return 4;
		break;
	case e_explain_5:
	case e_quiz_5:
		return 5;
		break;
	case e_explain_6:
	case e_quiz_6:
		return 6;
		break;
	case e_explain_7:
	case e_quiz_7:
		return 7;
		break;
	case e_explain_8:
	case e_quiz_8:
		return 8;
		break;
	case e_explain_9:
	case e_quiz_9:
		return 9;
		break;
	default:
		return 2;
	}
}

void game_multiplication::select_terms()
{
	_new_layout = _prev_layout;
	unsigned int fixed = fixed_term();
	if (_gen_res.prize_level == e_final_quiz) {
		_new_layout.first = 2 + rand()%8;
		_new_layout.second = 2 + rand()%8;
		return;
	}
	if (stage_is_quiz()) {
		_new_layout.first = 2 + rand()%9;
		_new_layout.second = fixed;
		if (rand()%2 == 0)
			std::swap(_new_layout.first, _new_layout.second);
	}
	else {
		if (_new_layout.second == 10) {
			advance_stage();
			select_terms();
		}
		else if (_new_layout.first < 10 && _new_layout.second == fixed)
			++_new_layout.first;
		else {
			++_new_layout.second;
			_new_layout.first = fixed;
		}
	}
}

void game_multiplication::advance_stage()
{
	if (_gen_res.prize_level == e_stage_count)
		return;
	++_gen_res.prize_level;
	_gen_res.show_prize = true;
	_select_term_counter = 0;
	_right_answers = 0;
	if (!stage_is_quiz())
		_prev_layout(1, fixed_term());
}

void game_multiplication::simplify_layout()
{
	if (_new_layout.first == _new_layout.second)
		_new_layout.second = 2;
	else if (_new_layout.first < _new_layout.second)
		_new_layout.second = _new_layout.first;
	else
		_new_layout.first = _new_layout.second;
}

void game_multiplication::advance_to_failed()
{
	if (_new_layout.first < _failed_layout.first)
		++_new_layout.first;
	else if (_new_layout.second < _failed_layout.second)
		++_new_layout.second;
}

bool game_multiplication::stage_is_quiz()
{
	switch (_gen_res.prize_level)
	{
	case e_quiz_2:
	case e_quiz_3:
	case e_quiz_4:
	case e_quiz_5:
	case e_quiz_6:
	case e_quiz_7:
	case e_quiz_8:
	case e_quiz_9:
	case e_final_quiz:
	case e_stage_count:
		return true;
	default:
		return false;
	}
}

unsigned int game_multiplication::required_right_answers()
{
	switch(_gen_res.prize_level)
	{
	case e_explain_2:
		return 17;
	case e_explain_3:
		return 16;
	case e_explain_4:
		return 15;
	case e_explain_5:
		return 14;
	case e_explain_6:
		return 13;
	case e_explain_7:
		return 12;
	case e_explain_8:
		return 11;
	case e_explain_9:
		return 10;
	case e_quiz_2:
	case e_quiz_3:
	case e_quiz_4:
	case e_quiz_5:
	case e_quiz_6:
	case e_quiz_7:
	case e_quiz_8:
	case e_quiz_9:
		return game_layout::required_right_answers();
	default:
		return 25;
	}
 }
